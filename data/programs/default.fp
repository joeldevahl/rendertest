#if defined(DX11) || defined(DX12)

cbuffer material : register( b2 )
{
	float4 material_color;
};

struct FP_OUTPUT
{
	float4 color : SV_TARGET;
};

void main(out FP_OUTPUT output)
{
	output.color = material_color;
}

#else

layout(std430, binding = 0, set = 2) buffer vpl2 { float4 material_color; };

layout(location = 0) out vec4 out_color;

void main()
{
	out_color = material_color;
}

#endif
