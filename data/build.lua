Unit:Using("vertex_layout")
Unit:Using("mesh")
Unit:Using("model")
Unit:Using("material")
Unit:Using("texture")
Unit:Using("vgpu")
Unit:Using("renderc")

function Unit.BuildTarget(self)

	local gpu_programs = {
		"data/programs/*.vp",
		"data/programs/*.fp",
	}
	for _,d in ipairs(gpu_programs) do
		local backend_progs = Compile(self.settings, Collect(d))
		local bundle = CompileGPUProgramBundle(self.settings, backend_progs)
		self:AddProduct(bundle)
	end

	local data = {
		"data/vertex_layouts/*.vl",
		"data/meshes/*.mesh",
		"data/materials/*.material",
		"data/models/*.model",
		"data/state_blocks/*.state_block",
		"data/render_targets/*.render_target",
		"data/render_passes/*.render_pass",
		"data/pipelines/*.pipeline",
		"data/pipeline_sets/*.pipeline_set",
		"data/render_scripts/*.render_script",
	}
	for _,d in ipairs(data) do
		for _,r in pairs(Compile(self.settings, Collect(d))) do
			self:AddProduct(r)
		end
	end

	-- TODO: don't write this all the time
	os.execute("mkdir -p " .. PathJoin(target.outdir, "data"))
	file = io.open(PathJoin(target.outdir, "data/precache.txt"), "wb")
	for v in TableWalk(self.target_products[target]) do
		file:write(string.gsub(v, target.outdir .. "/", "") .. "\n")
	end
	file:close()
end
