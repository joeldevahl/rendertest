#include <core/assert.h>
#include <core/hash.h>
#include <memory/allocator.h>
#include <resource/resource_cache.h>

#include <vgpu.h>
#include <renderc/types.h>

#include "render_target_creator.h"

static bool render_target_create(void* context, allocator_t* allocator, void* creation_data, size_t size, void** out_resource_data, void** out_private_data)
{
	resource_context_t* resource_context = (resource_context_t*)context;
	vgpu_device_t* device = resource_context->device;

	renderc_render_target_data_t* data = (renderc_render_target_data_t*)creation_data;

	vgpu_create_texture_params_t params =
	{
		VGPU_TEXTURETYPE_2D,
		(vgpu_texture_format_t)data->format,
		VGPU_USAGE_DEFAULT,
		data->flags ? 1280u : data->width,
		data->flags ? 720u : data->height,
		1u,
		1u,
		true,
		"loaded render target", // TODO: real name here
		{ 1.0f, 0.0f, 0.0f, 1.0f }
	};
	*out_resource_data = vgpu_create_texture(device, &params);

	return true;
}

static bool render_target_recreate(void* context, allocator_t* allocator, void* creation_data, size_t size, void* prev_resource_data, void* prev_private_data, void** out_resource_data, void** out_private_data)
{
	return false;
}

static void render_target_destroy(void* context, allocator_t* allocator, void* resource_data, void* private_data)
{
	resource_context_t* resource_context = (resource_context_t*)context;
	vgpu_device_t* device = resource_context->device;
	vgpu_texture_t* render_target = (vgpu_texture_t*)resource_data;
	vgpu_destroy_texture(device, render_target);
}

void render_target_register_creator(resource_context_t* resource_context)
{
	resource_creator_t render_target_creator = {
		render_target_create,
		render_target_recreate,
		render_target_destroy,
		&allocator_default,
		resource_context,
		hash_string("render_target")
	};
	resource_cache_add_creator(resource_context->resource_cache, &render_target_creator);
}
