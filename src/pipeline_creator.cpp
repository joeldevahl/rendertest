#include <core/assert.h>
#include <core/hash.h>
#include <memory/allocator.h>
#include <resource/resource_cache.h>

#include <renderc/types.h>
#include <riff/types.h>

#include "resource_context.h"

#include <cstring>

struct pipeline_resource_private_t
{
	resource_handle_t vp_handle;
	resource_handle_t fp_handle;
};

static bool pipeline_create(void* context, allocator_t* allocator, void* creation_data, size_t size, void** out_resource_data, void** out_private_data)
{
	resource_context_t* resource_context = (resource_context_t*)context;
	resource_cache_t* resource_cache = resource_context->resource_cache;

	uint8_t* data_raw = (uint8_t*)creation_data;

	riff_header_t* header = (riff_header_t*)creation_data;
	data_raw += sizeof(*header);

	ASSERT(riff_test_id(header, 'P', 'I', 'P', 'E'), "Wrong magic for pipeline");
	ASSERT(header->size >= size - sizeof(*header), "File size less then encoded size in file");

	*out_resource_data = ALLOCATOR_ALLOC_TYPE(allocator, renderc_pipeline_data_t);
	memcpy(*out_resource_data, data_raw, sizeof(renderc_pipeline_data_t));

	return true;
}

static bool pipeline_recreate(void* context, allocator_t* allocator, void* creation_data, size_t size, void* prev_resource_data, void* prev_private_data, void** out_resource_data, void** out_private_data)
{
	return false;
}

static void pipeline_destroy(void* context, allocator_t* allocator, void* resource_data, void* private_data)
{
	ALLOCATOR_FREE(allocator, resource_data);
}

void pipeline_register_creator(resource_context_t* resource_context)
{
	resource_creator_t pipeline_creator = {
		pipeline_create,
		pipeline_recreate,
		pipeline_destroy,
		&allocator_default,
		resource_context,
		hash_string("pipeline")
	};
	resource_cache_add_creator(resource_context->resource_cache, &pipeline_creator);
}
