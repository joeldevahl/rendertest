#include <core/assert.h>
#include <core/hash.h>
#include <memory/allocator.h>
#include <resource/resource_cache.h>

#include <renderc/types.h>
#include <riff/types.h>

#include "resource_context.h"

#include <cstring>

static bool state_block_create(void* context, allocator_t* allocator, void* creation_data, size_t size, void** out_resource_data, void** out_private_data)
{
	resource_context_t* resource_context = (resource_context_t*)context;

	uint8_t* data_raw = (uint8_t*)creation_data;

	riff_header_t* header = (riff_header_t*)creation_data;
	data_raw += sizeof(*header);
	size_t offset = 0;

	ASSERT(riff_test_id(header, 'S', 'T', 'A', 'T'), "Wrong magic for state block");
	ASSERT(header->size >= size - sizeof(*header), "File size less then encoded size in file");

	*out_resource_data = ALLOCATOR_ALLOC_TYPE(allocator, renderc_state_block_data_t);
	memcpy(*out_resource_data, data_raw, sizeof(renderc_state_block_data_t));

	return true;
}

static bool state_block_recreate(void* context, allocator_t* allocator, void* creation_datd, size_t size, void* prev_resource_data, void* prev_private_data, void** out_resource_data, void** out_private_data)
{
	return false;
}

static void state_block_destroy(void* context, allocator_t* allocator, void* resource_data, void* private_data)
{
	ALLOCATOR_FREE(allocator, resource_data);
}

void state_block_register_creator(resource_context_t* resource_context)
{
		resource_creator_t state_block_creator = {
			state_block_create,
			state_block_recreate,
			state_block_destroy,
			&allocator_default,
			resource_context,
			hash_string("state_block")
		};
		resource_cache_add_creator(resource_context->resource_cache, &state_block_creator);
}
