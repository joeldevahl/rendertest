#include <core/assert.h>
#include <core/hash.h>
#include <memory/allocator.h>
#include <resource/resource_cache.h>

#include <vgpu.h>
#include <riff/types.h>

#include "program_creator.h"

#include <cstring>

static vgpu_create_program_params_t parse_program_bundle(void* data, allocator_t* allocator, vgpu_device_type_t device_type)
{
	uint8_t* start = (uint8_t*)data;

	uint64_t* count = (uint64_t*)start; start += sizeof(uint64_t);
	for (uint64_t i = 0; i < *count; ++i)
	{
		uint64_t* size = (uint64_t*)start; start += sizeof(uint64_t);
		uint64_t* type = (uint64_t*)start; start += sizeof(uint64_t);
		uint64_t* program_type = (uint64_t*)start; start += sizeof(uint64_t);
		if (*type == device_type)
		{
			vgpu_create_program_params_t res;
			res.data = start;
			res.size = *size;
			res.program_type = (vgpu_program_type_t)*program_type;
			return res;
		}
		start = (uint8_t*)ALIGN_UP(start + *size, sizeof(uint64_t));
	}

	BREAKPOINT();
	vgpu_create_program_params_t res = { };
	return res;
}

static bool program_create(void* context, allocator_t* allocator, void* creation_data, size_t size, void** out_resource_data, void** out_private_data)
{
	resource_context_t* resource_context = (resource_context_t*)context;
	vgpu_device_t* device = resource_context->device;
	vgpu_device_type_t device_type = vgpu_get_device_type(device);

	vgpu_create_program_params_t create_params = parse_program_bundle(creation_data, allocator, device_type);

	vgpu_program_t* prog = vgpu_create_program(device, &create_params);

	*out_resource_data = prog;

	return true;
}

static bool program_recreate(void* context, allocator_t* allocator, void* creation_data, size_t size, void* prev_resource_data, void* prev_private_data, void** out_resource_data, void** out_private_data)
{
	return false;
}

static void program_destroy(void* context, allocator_t* allocator, void* resource_data, void* private_data)
{
	resource_context_t* resource_context = (resource_context_t*)context;
	vgpu_device_t* device = resource_context->device;
	vgpu_program_t* program = (vgpu_program_t*)resource_data;
	vgpu_destroy_program(device, program);
}

void program_register_creator(resource_context_t* resource_context)
{
	resource_creator_t program_creator = {
		program_create,
		program_recreate,
		program_destroy,
		&allocator_default,
		resource_context,
		hash_string("vpb")
	};
	resource_cache_add_creator(resource_context->resource_cache, &program_creator);

	program_creator.type_hash = hash_string("fpb");
	resource_cache_add_creator(resource_context->resource_cache, &program_creator);
}
