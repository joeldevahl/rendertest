#pragma once

struct resource_context_t
{
	struct application_s* application;
	struct window_s* window;
	struct vfs_s* vfs;
	struct dl_context* dl_ctx;
	struct vgpu_device_s* device;
	struct vgpu_resource_layout_s* resource_layout;
	struct render_s* render;
	struct resource_cache_s* resource_cache;
	struct model_system_s* model_system;
};
