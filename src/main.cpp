#include <core/assert.h>
#include <core/defines.h>
#include <core/time.h>
#include <core/hash.h>
#include <memory/allocator.h>
#include <memory/memory.h>
#include <application/application.h>
#include <application/window.h>
#include <container/array.h>
#include <vgpu.h>
#include <render/rendercommand.h>
#include <render/render.h>
#include <container/idpool.h>
#include <math/mat4.h>
#include <io/file.h>
#include <io/vfs.h>
#include <io/vfs_mount_fs.h>
#include <resource/resource_cache.h>

#include <vgpu.h>

#include "resource_context.h"
#include "mesh_creator.h"
#include "material_creator.h"
#include "model_creator.h"
#include "program_creator.h"
#include "state_block_creator.h"
#include "render_target_creator.h"
#include "render_pass_creator.h"
#include "pipeline_creator.h"
#include "pipeline_set_creator.h"
#include "render_script_creator.h"
#include "render_script_executor.h"
#include "model_system.h"

#include <dl/dl.h>

#include <cstdio>

#define DATA_PATH "local/build/" PLATFORM_STRING "/"
#define MAX_ENTITIES 10000

#if defined(FAMILY_WINDOWS)
#	include <core/windows/windows.h>
#endif

static void log_func(const char* fmt, ...)
{
	char buffer[2048];
	va_list list;
	va_start(list, fmt);
	vsnprintf(buffer, 2048, fmt, list);
	va_end(list);
	buffer[2048 - 1] = 0;
#ifdef FAMILY_WINDOWS
	OutputDebugStringA(buffer);
	OutputDebugStringA("\n");
#endif
	fputs(buffer, stderr);
	fputs("\n", stderr);
}

static int error_func(const char* file, unsigned int line, const char* cond, const char* fmt, ...)
{

	char buffer[2048];
	va_list list;
	va_start(list, fmt);
	vsnprintf(buffer, 2048, fmt, list);
	va_end(list);
	buffer[2048 - 1] = 0;
#ifdef FAMILY_WINDOWS
	OutputDebugStringA(buffer);
	OutputDebugStringA("\n");
#endif
	fputs(buffer, stderr);
	fputs("\n", stderr);

	return 1;
}


int atec_main(application_t* application)
{
	resource_context_t resource_context;
	memory_zero(&resource_context, sizeof(resource_context));
	resource_context.application = application;

	window_create_params_t window_create_params;
	window_create_params.allocator = &allocator_default;
	window_create_params.width = 1280;
	window_create_params.height = 720;
	window_create_params.name = "rendertest";
	window_t* window = window_create(&window_create_params);
	resource_context.window = window;

	vfs_create_params_t vfs_params;
	vfs_params.allocator = &allocator_default;
	vfs_params.max_mounts = 4;
	vfs_params.max_requests = 32;
	vfs_params.buffer_size = 16 * 1024 * 1024; // 16 MiB
	vfs_t* vfs = vfs_create(&vfs_params);
	resource_context.vfs = vfs;

	vfs_mount_fs_t* vfs_fs_data = vfs_mount_fs_create(&allocator_default, DATA_PATH);
	vfs_result_t vfs_res = vfs_add_mount(vfs, vfs_mount_fs_read_func, vfs_fs_data);
	ASSERT(vfs_res == VFS_RESULT_OK, "failed to add vfs mount");

	dl_ctx_t dl_ctx;
	dl_create_params_t dl_create_params;
	DL_CREATE_PARAMS_SET_DEFAULT(dl_create_params);
	dl_error_t err = dl_context_create(&dl_ctx, &dl_create_params);
	resource_context.dl_ctx = dl_ctx;

	resource_cache_create_params_t resource_cache_params;
	resource_cache_params.allocator = &allocator_default;
	resource_cache_params.vfs = vfs;
	resource_cache_params.max_resources = 1024;
	resource_cache_params.max_resource_handles = 4096;
	resource_cache_params.max_creators = 16;
	resource_cache_params.max_callbacks = 16;
	resource_cache_t* resource_cache = resource_cache_create(&resource_cache_params);
	resource_context.resource_cache = resource_cache;

	vgpu_create_device_params_t device_create_params = {
		(vgpu_allocator_t*)&allocator_default,
		window_get_platform_handle(window),
		0, //GPU_CAPS_FLAG_BIND_CONSTANT_BUFFER_AT_OFFSET, // Enable this to force dx11 style constant buffers (one per instance)
		log_func,
		error_func,
	};
	vgpu_device_t* device = vgpu_create_device(&device_create_params);
	resource_context.device = device;

	vgpu_create_thread_context_params_t thread_context_params =
	{
	};
	vgpu_thread_context_t* thread_context = vgpu_create_thread_context(device, &thread_context_params);
	vgpu_prepare_thread_context(device, thread_context);

	vgpu_caps_t caps = {};
	vgpu_get_caps(device, &caps);

	vgpu_create_command_list_params_t command_list_params =
	{
		VGPU_COMMAND_LIST_GRAPHICS,
	};
	vgpu_command_list_t* command_list = vgpu_create_command_list(device, &command_list_params);

	vgpu_resource_layout_slot_t resource_slots[4];
	memset(resource_slots, 0, sizeof(resource_slots));
	resource_slots[0].type = VGPU_RESOURCE_SLOT_TYPE_RESOURCE;
	resource_slots[0].resource.location = 0;
	resource_slots[0].resource.treat_as_constant_buffer = true;
	resource_slots[1].type = VGPU_RESOURCE_SLOT_TYPE_TABLE;
	resource_slots[1].table.range_buffers.start = 1;
	resource_slots[1].table.range_buffers.count = 1;
	resource_slots[2].type = VGPU_RESOURCE_SLOT_TYPE_TABLE;
	resource_slots[2].table.range_constant_buffers.start = 2;
	resource_slots[2].table.range_constant_buffers.count = 1;
	resource_slots[3].type = VGPU_RESOURCE_SLOT_TYPE_TABLE;
	resource_slots[3].table.range_constant_buffers.start = 3;
	resource_slots[3].table.range_constant_buffers.count = 1;
	vgpu_resource_layout_t* resource_layout = vgpu_create_resource_layout(device, resource_slots, ARRAY_LENGTH(resource_slots));
	resource_context.resource_layout = resource_layout;

	render_create_params_t render_create_params;
	render_create_params.allocator = &allocator_default;
	render_create_params.device = device;
	render_create_params.command_list = command_list;
	render_t* render = render_create(&render_create_params);
	resource_context.render = render;

	render_script_executor_t* render_script_executor = render_script_executor_create(&allocator_default, render, device, thread_context, command_list);

	mesh_register_creator(&resource_context);
	material_register_creator(&resource_context);
	model_register_creator(&resource_context);
	program_register_creator(&resource_context);
	state_block_register_creator(&resource_context);
	render_target_register_creator(&resource_context);
	render_pass_register_creator(&resource_context);
	pipeline_register_creator(&resource_context);
	pipeline_set_register_creator(&resource_context);
	render_script_register_creator(&resource_context);

	model_system_create_params_t model_system_params = {
		&allocator_default,
		device,
		resource_layout,
		32,
		MAX_ENTITIES,
	};
	model_system_t* model_system = model_system_create(&model_system_params);
	resource_context.model_system = model_system;
	render_script_subsystem_t model_subsystem = {
		model_system_query,
		model_system,
	};
	render_script_executor_add_subsystem(render_script_executor, "model", &model_subsystem);

	char* precache_text = nullptr;
	size_t precache_size = 0;
	vfs_sync_read(vfs, &allocator_default, "data/precache.txt", (void**)&precache_text, &precache_size);
	precache_text[precache_size - 1] = '\0';

	vgpu_begin_command_list(thread_context, command_list, nullptr);

	array_t<resource_handle_t> handles(&allocator_default, 128);
	char* precache_name = strtok(precache_text, "\r\n");
	while (precache_name != nullptr)
	{
		const char* type_str = strrchr(precache_name, '.') + 1;
		if (strcmp(type_str, "vl") != 0) // TODO: remove this early out and select what to precache better
		{
			uint32_t name_hash = hash_string(precache_name);
			uint32_t type_hash = hash_string(type_str);

			vfs_request_t request;
			vfs_res = vfs_begin_request(vfs, precache_name, &request);
			ASSERT(vfs_res == VFS_RESULT_OK, "vfs failed to begin request");

			vfs_res = vfs_request_wait_not_pending(vfs, request);
			ASSERT(vfs_res == VFS_RESULT_OK, "vfs failed request");

			uint8_t* data = NULL;
			size_t size = 0;
			vfs_res = vfs_request_data(vfs, request, (void**)&data, &size);
			ASSERT(vfs_res == VFS_RESULT_OK, "vfs failed request");

			ASSERT(data, "No data loaded");

			resource_handle_t handle;
			resource_cache_result_t resource_res = resource_cache_create_resource(resource_cache, name_hash, type_hash, data, size, &handle);
			ASSERT(resource_res == RESOURCE_CACHE_RESULT_OK);
			handles.append(handle);

			vfs_res = vfs_end_request(vfs, request);
			ASSERT(vfs_res == VFS_RESULT_OK, "vfs failed to end request");
		}
		precache_name = strtok(nullptr, "\r\n");
	}
	ALLOCATOR_FREE(&allocator_default, precache_text);

	mat4_t projection_matrix = perspective(45.0f, 1280.0f/720.0f, 0.1f, 1000.0f);
	mat4_t view_matrix = lookat(vec3_t(0.0f, 0.0f, 150.0f), vec3_t(0.0f, 0.0f, 0.0f), vec3_t(0.0f, 1.0f, 0.0f));
	mat4_t projection_view_matrix = projection_matrix * view_matrix;

	vgpu_create_buffer_params_t global_buffer_params;
	global_buffer_params.num_bytes = sizeof(mat4_t) * 4; // need some padding here :/
	global_buffer_params.usage = VGPU_USAGE_DEFAULT;
	global_buffer_params.flags = VGPU_BUFFER_FLAG_CONSTANT_BUFFER;
	global_buffer_params.structure_stride = sizeof(mat4_t) * 4; // need some padding here :/
	global_buffer_params.name = "global_buffer";
	vgpu_buffer_t* global_buffer = vgpu_create_buffer(device, &global_buffer_params);
	vgpu_resource_table_entry_t global_entry = {
		3,
		VGPU_RESOURCE_BUFFER,
		global_buffer,
		0,
		sizeof(mat4_t)*4, // need some padding here :/
		true
	};
	vgpu_resource_table_t* global_resource_table = vgpu_create_resource_table(device, resource_context.resource_layout, 3, &global_entry, 1);

	vgpu_set_buffer_data(device, global_buffer, 0, &projection_view_matrix, sizeof(mat4_t));

	resource_handle_t render_script_handle;
	resource_cache_result_t resource_res = resource_cache_get_by_name(resource_cache, "data/render_scripts/default.render_script", &render_script_handle);
	ASSERT(resource_res == RESOURCE_CACHE_RESULT_OK);
	render_script_t* render_script;
	resource_cache_handle_handle_to_pointer(resource_cache, render_script_handle, (void**)&render_script);

	render_script_view_context_t view_context = {
		global_resource_table,
	};
	render_script_executor_add_view_context(render_script_executor, "main", &view_context);

	resource_handle_t model_handle;
	resource_res = resource_cache_get_by_name(resource_cache, "data/models/plane_xy.bmodel", &model_handle);
	ASSERT(resource_res == RESOURCE_CACHE_RESULT_OK);
	model_t* model;
	resource_cache_handle_handle_to_pointer(resource_cache, model_handle, (void**)&model);

	model_instance_t model_instances[MAX_ENTITIES];
	model_instance_data_t model_instance_data;
	for (size_t i = 0; i < MAX_ENTITIES; ++i)
	{
		float x = (float)(i % 100) - 50;
		float y = (float)(i / 100) - 50;
		mat4_t mat = translate(x, y, 0.0f) * rotate_x(x * 0.01f) * rotate_y(y * 0.01f);
		memcpy(model_instance_data.transform, &mat, sizeof(model_instance_data.transform));
		model_instances[i] = model_system_create_instance(model_system, model, &model_instance_data);
	}

	vgpu_end_command_list(command_list);
	vgpu_apply_command_lists(device, 1, &command_list, 0);

	uint32_t t = 0;
	uint64_t time_acc_draw = 0;
	uint64_t time_acc_model = 0;
	uint64_t time_acc_pass = 0;
	uint64_t time_acc_device = 0;
	while (t < 1000)
	{
		application_update(application);

		uint64_t start = time_current();

		model_instance_data_t model_instance_data;
		for (size_t i = 0; i < MAX_ENTITIES; ++i)
		{
			float x = (float)(i % 100) - 50;
			float y = (float)(i / 100) - 50;
			mat4_t mat = translate(x, y, 0.0f) * rotate_x((x + t) * 0.01f) * rotate_y((y + t) * 0.01f);
			memcpy(model_instance_data.transform, &mat, sizeof(model_instance_data.transform));
			model_system_set_instance_data(model_system, model_instances[i], &model_instance_data);
		}

		uint64_t end_model = time_current();

		render_script_executor_run_script(render_script_executor, render_script);

		uint64_t end = time_current();

		render_stats_t stats;
		render_get_frame_stats(render, &stats);
		time_acc_draw += end - start;
		time_acc_model += end_model - start;
		time_acc_pass += stats.total_cpu_draw_time;
		time_acc_device += stats.device_cpu_draw_time;

		vgpu_present(device);
		vgpu_prepare_thread_context(device, thread_context);

		t += 1;
	}

	uint64_t freq = time_frequency();
	render_stats_t stats;
	render_get_frame_stats(render, &stats);
	FILE* f = fopen("log.txt", "w+");
	fprintf(f, "draw elapsed %f ms per frame (%d ticks at %d Hz)\n", (float)time_acc_draw / (float)freq, (int32_t)time_acc_draw, (int32_t)freq);
	fprintf(f, "model elapsed %f ms per frame (%d ticks at %d Hz)\n", (float)time_acc_model / (float)freq, (int32_t)time_acc_model, (int32_t)freq);
	fprintf(f, "pass elapsed %f ms per frame (%d ticks at %d Hz)\n", (float)time_acc_pass / (float)freq, (int32_t)time_acc_pass, (int32_t)freq);
	fprintf(f, "device elapsed %f ms per frame (%d ticks at %d Hz)\n", (float)time_acc_device / (float)freq, (int32_t)time_acc_device, (int32_t)freq);
	fprintf(f, "max render memory consumed %d KiB\n", (int)stats.max_consumed / 1024);
	fclose(f);

	for(size_t i = 0; i < MAX_ENTITIES; ++i)
		model_system_destroy_instance(model_system, model_instances[i]);

	resource_cache_release_handle(resource_cache, model_handle);

	vgpu_destroy_resource_table(device, global_resource_table);
	vgpu_destroy_buffer(device, global_buffer);

	resource_res = resource_cache_release_handle(resource_cache, render_script_handle);
	ASSERT(resource_res == RESOURCE_CACHE_RESULT_OK);

	for(size_t i = 0; i < handles.length(); ++i)
	{
		resource_cache_result_t resource_res = resource_cache_release_handle(resource_cache, handles[i]);
		ASSERT(resource_res == RESOURCE_CACHE_RESULT_OK);
	}

	model_system_destroy(model_system);

	render_script_executor_destroy(render_script_executor);

	render_destroy(render);

	vgpu_destroy_resource_layout(device, resource_layout);
	vgpu_destroy_command_list(device, command_list);
	vgpu_destroy_thread_context(device, thread_context);
	vgpu_destroy_device(device);

	resource_cache_destroy(resource_cache);

	vfs_mount_fs_destroy(vfs_fs_data);
	vfs_destroy(vfs);

	window_destroy(window);

	return 0;
}
