#include <core/assert.h>
#include <core/hash.h>
#include <memory/allocator.h>
#include <resource/resource_cache.h>

#include <units/model/types/model.h>

#include "model_system.h"
#include "model_creator.h"
#include "mesh_creator.h"
#include "material_creator.h"
#include "resource_context.h"

#include <dl/dl.h>

#include <units/model/types/model.h>

static const unsigned char model_typelib[] =
{
#include <units/model/types/model.tlb.hex>
};

static bool model_create(void* context, allocator_t* allocator, void* creation_data, size_t size, void** out_resource_data, void** out_private_data)
{
	resource_context_t* resource_context = (resource_context_t*)context;

	model_data_t* model_data = nullptr;
	size_t consumed = 0;
	dl_error_t err = dl_instance_load_inplace(
			resource_context->dl_ctx,
			model_data_t::TYPE_ID,
			(unsigned char*)creation_data,
			size,
			(void**)&model_data,
			&consumed);
	ASSERT(err == DL_ERROR_OK);

	resource_cache_result_t resource_res;
	resource_handle_t mesh_handle;
	resource_res = resource_cache_get_by_hash(resource_context->resource_cache, model_data->mesh_hash, &mesh_handle);
	ASSERT(resource_res == RESOURCE_CACHE_RESULT_OK);
	mesh_s* mesh = nullptr;
	resource_cache_handle_handle_to_pointer(resource_context->resource_cache, mesh_handle, (void**)&mesh);

	resource_handle_t material_handle;
	resource_res = resource_cache_get_by_hash(resource_context->resource_cache, model_data->material_hash, &material_handle);
	ASSERT(resource_res == RESOURCE_CACHE_RESULT_OK);
	material_s* material = nullptr;
	resource_cache_handle_handle_to_pointer(resource_context->resource_cache, material_handle, (void**)&material);

	model_create_data_t model_create_data = {
		mesh->index_buffer,
		mesh->index_type,
		mesh->draw_count,
		mesh->resource_table,
		material->resource_table
	};
	model_t* model = model_system_create_model(resource_context->model_system, &model_create_data);

	// TODO: store private data
	*out_resource_data = (void*)model;
	return true;
}

static bool model_recreate(void* context, allocator_t* allocator, void* creation_data, size_t size, void* prev_resource_data, void* prev_private_data, void** out_resource_data, void** out_private_data)
{
	return false;
}

static void model_destroy(void* context, allocator_t* allocator, void* resource_data, void* private_data)
{
	resource_context_t* resource_context = (resource_context_t*)context;
	model_t* model = (model_t*)resource_data;

	// TODO: release mesh handle
	model_system_destroy_model(resource_context->model_system, model);
}

void model_register_creator(resource_context_t* resource_context)
{
	dl_context_load_type_library(resource_context->dl_ctx, model_typelib, ARRAY_LENGTH(model_typelib));

	resource_creator_t model_creator = {
		model_create,
		model_recreate,
		model_destroy,
		&allocator_default,
		resource_context,
		hash_string("bmodel")
	};
	resource_cache_add_creator(resource_context->resource_cache, &model_creator);
}
