#include <core/assert.h>
#include <core/hash.h>
#include <memory/allocator.h>
#define VGPU_TRANSLATION_UTILS
#include <vgpu.h>
#include <resource/resource_cache.h>

#include <units/mesh/types/mesh.h>

#include "mesh_creator.h"
#include "resource_context.h"

#include <dl/dl.h>

#include <units/mesh/types/mesh.h>


static const unsigned char mesh_typelib[] =
{
#include <units/mesh/types/mesh.tlb.hex>
};

static bool mesh_create(void* context, allocator_t* allocator, void* creation_data, size_t size, void** out_resource_data, void** out_private_data)
{
	resource_context_t* resource_context = (resource_context_t*)context;
	vgpu_device_t* device = resource_context->device;
	mesh_s* mesh = ALLOCATOR_NEW(allocator, mesh_s);

	mesh_data_t* mesh_data = nullptr;
	size_t consumed = 0;
	dl_error_t err = dl_instance_load_inplace(
			resource_context->dl_ctx,
			mesh_data_t::TYPE_ID,
			(unsigned char*)creation_data,
			size,
			(void**)&mesh_data,
			&consumed);
	ASSERT(err == DL_ERROR_OK);

	vgpu_create_buffer_params_t vertex_buffer_params;
	vertex_buffer_params.num_bytes = ALIGN_UP(mesh_data->vertex_data.data.count, 256);
	vertex_buffer_params.usage = VGPU_USAGE_DEFAULT;
	vertex_buffer_params.flags = 0;
	vertex_buffer_params.structure_stride = 16;
	vertex_buffer_params.name = "vb";
	mesh->vertex_buffer = vgpu_create_buffer(device, &vertex_buffer_params);
	vgpu_set_buffer_data(device, mesh->vertex_buffer, 0, &mesh_data->vertex_data.data[0], mesh_data->vertex_data.data.count);

	mesh->index_type = (vgpu_data_type_t)mesh_data->index_data.data_type;

	vgpu_create_buffer_params_t index_buffer_params;
	index_buffer_params.num_bytes = mesh_data->index_data.data.count;
	index_buffer_params.usage = VGPU_USAGE_DEFAULT;
	index_buffer_params.flags = VGPU_BUFFER_FLAG_INDEX_BUFFER;
	index_buffer_params.structure_stride = 0;
	index_buffer_params.name = "ib";
	mesh->index_buffer = vgpu_create_buffer(device, &index_buffer_params);
	vgpu_set_buffer_data(device, mesh->index_buffer, 0, &mesh_data->index_data.data[0], mesh_data->index_data.data.count);

	mesh->draw_count = index_buffer_params.num_bytes / vgpu_data_type_size(mesh->index_type);

	vgpu_resource_table_entry_t vb_entry = {
		1,
		VGPU_RESOURCE_BUFFER,
		mesh->vertex_buffer,
		0,
		vertex_buffer_params.num_bytes,
		false
	};
	mesh->resource_table = vgpu_create_resource_table(device, resource_context->resource_layout, 1, &vb_entry, 1);

	*out_resource_data = (void*)mesh;
	return true;
}

static bool mesh_recreate(void* context, allocator_t* allocator, void* creation_data, size_t size, void* prev_resource_data, void* prev_private_data, void** out_resource_data, void** out_private_data)
{
	return false;
}

static void mesh_destroy(void* context, allocator_t* allocator, void* resource_data, void* private_data)
{
	resource_context_t* resource_context = (resource_context_t*)context;
	vgpu_device_t* device = resource_context->device;
	mesh_s* mesh = (mesh_s*)resource_data;

	vgpu_destroy_resource_table(device, mesh->resource_table);
	vgpu_destroy_buffer(device, mesh->index_buffer);
	vgpu_destroy_buffer(device, mesh->vertex_buffer);

	ALLOCATOR_DELETE(allocator, mesh_s, mesh);
}

void mesh_register_creator(resource_context_t* resource_context)
{
	dl_context_load_type_library(resource_context->dl_ctx, mesh_typelib, ARRAY_LENGTH(mesh_typelib));

	resource_creator_t mesh_creator = {
		mesh_create,
		mesh_recreate,
		mesh_destroy,
		&allocator_default,
		resource_context,
		hash_string("mesh")
	};
	resource_cache_add_creator(resource_context->resource_cache, &mesh_creator);
}
