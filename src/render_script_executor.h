#pragma once

#include "render_script_creator.h"

#include <memory/allocator.h>
#include <render/types.h>

struct render_script_view_context_t
{
	vgpu_resource_table_t* resource_table;
};

struct render_script_query_context_t
{
	render_script_view_context_t* view;
};

typedef render_instance_t* (*render_script_subsystem_query_t)(void* user_pointer, const render_script_query_context_t* context, size_t* out_num_instances);

struct render_script_subsystem_t
{
	render_script_subsystem_query_t query_func;
	void* user_pointer;
};

struct render_script_executor_t;

render_script_executor_t* render_script_executor_create(allocator_t* allocator, render_t* render, vgpu_device_t* device, vgpu_thread_context_t* thread_context, vgpu_command_list_t* command_list);

void render_script_executor_destroy(render_script_executor_t* executor);

void render_script_executor_add_subsystem(render_script_executor_t* executor, const char* name, const render_script_subsystem_t* subsystem);

void render_script_executor_add_view_context(render_script_executor_t* executor, const char* name, const render_script_view_context_t* view_context);

void render_script_executor_run_script(render_script_executor_t* executor, render_script_t* script);
