#pragma once

#include <render/types.h>

struct mesh_s
{
	vgpu_resource_table_t* resource_table;
	size_t draw_count;
	vgpu_buffer_t* index_buffer;
	vgpu_data_type_t index_type;
	vgpu_buffer_t* vertex_buffer;
	uint64_t vertex_layout;
};

void mesh_register_creator(struct resource_context_t* resource_context);
