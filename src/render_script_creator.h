#pragma once

#include <core/defines.h>
#include <render/rendercommand.h>

struct render_script_t
{
	size_t commands_size;
	void* commands;
};

struct ALIGN(16) renderse_command_query_t
{
	render_command_header_t header;

	render_pipeline_set_t* pipeline_set;
	vgpu_resource_table_t* global_resource_table;

	uint32_t and_mask;
	uint32_t not_mask;

	uint32_t view_context_name_hash;

	size_t num_subsystems;
	uint32_t subsystem_name_hashes[];
};

void render_script_register_creator(struct resource_context_t* resource_context);
