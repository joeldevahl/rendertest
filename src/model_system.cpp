#include <container/array.h>
#include <container/cobjpool.h>
#include <container/objpool.h>

#include <render/types.h>
#include <vgpu.h>

#include "model_system.h"

struct model_s
{
	model_create_data_t data;
	vgpu_resource_table_t* mesh_resource_table;
	vgpu_resource_table_t* material_resource_table;
	uint32_t ref_count;
};

struct model_instance_s
{
	union
	{
		vgpu_buffer_t* constant_buffer;
		model_instance_data_t data;
	};
	model_s* model;
};

struct model_system_s
{
	allocator_t* allocator;
	vgpu_device_t* device;
	vgpu_resource_layout_t* resource_layout;

	objpool_t<model_t, uint16_t> models;
	cobjpool_t<model_instance_s, model_instance_t> instances;
	array_t<render_instance_t> render_instances;

	vgpu_caps_t gpu_caps;
	vgpu_buffer_t* instance_buffer;
};

model_system_t* model_system_create(const model_system_create_params_t* params)
{
	model_system_t* system = ALLOCATOR_NEW(params->allocator, model_system_t);
	system->allocator = params->allocator;
	system->device = params->device;
	system->resource_layout = params->resource_layout;

	system->models.create(params->allocator, params->max_models);
	system->instances.create(params->allocator, params->max_model_instances);
	system->render_instances.create(params->allocator, params->max_model_instances);
	system->render_instances.set_length(params->max_model_instances);

	vgpu_get_caps(params->device, &system->gpu_caps);

	if (system->gpu_caps.flags & VGPU_CAPS_FLAG_BIND_CONSTANT_BUFFER_AT_OFFSET)
	{
		vgpu_create_buffer_params_t buffer_params;
		buffer_params.num_bytes = params->max_model_instances*sizeof(model_instance_data_t);
		buffer_params.usage = VGPU_USAGE_DYNAMIC;
		buffer_params.flags = VGPU_BUFFER_FLAG_CONSTANT_BUFFER;
		buffer_params.structure_stride = sizeof(model_instance_data_t);
		buffer_params.name = "model system instance buffer";
		system->instance_buffer = vgpu_create_buffer(params->device, &buffer_params);
	}

	return system;
}

void model_system_destroy(model_system_t* system)
{

	if (system->gpu_caps.flags & VGPU_CAPS_FLAG_BIND_CONSTANT_BUFFER_AT_OFFSET)
		vgpu_destroy_buffer(system->device, system->instance_buffer);

	ALLOCATOR_DELETE(system->allocator, model_system_t, system);
}

render_instance_t* model_system_query(void* user_pointer, const render_script_query_context_t* context, size_t* out_num_instances)
{
	model_system_t* system = (model_system_t*)user_pointer;
	const size_t num_instances = system->instances.num_used();

	const model_instance_s* model_instances = system->instances.base_ptr();
	render_instance_t* render_instances = system->render_instances.begin();

	bool instance_arr = system->gpu_caps.flags & VGPU_CAPS_FLAG_BIND_CONSTANT_BUFFER_AT_OFFSET;
	model_instance_data_t* instance_data = nullptr;
	vgpu_lock_buffer_params_t lock_params;
	if (instance_arr)
	{
		lock_params.buffer = system->instance_buffer;
		lock_params.offset = 0;
		lock_params.num_bytes = num_instances * sizeof(model_instance_data_t);
		instance_data = (model_instance_data_t*)vgpu_lock_buffer(system->device, &lock_params);
	}

	// TODO: could sort on model and render instanced
	for(size_t i = 0; i < num_instances; ++i)
	{
		const model_s* model = model_instances[i].model;

		render_instances[i].resource_slot_bindings[0].type = VGPU_RESOURCE_BUFFER;
		if (instance_arr)
		{
			memcpy(instance_data + i, &model_instances[i].data, sizeof(model_instance_data_t));
			render_instances[i].resource_slot_bindings[0].buffer.buffer = system->instance_buffer;
			render_instances[i].resource_slot_bindings[0].buffer.offset = i * sizeof(model_instance_data_t);
		}
		else
		{
			render_instances[i].resource_slot_bindings[0].buffer.buffer = model_instances[i].constant_buffer;
			render_instances[i].resource_slot_bindings[0].buffer.offset = 0;
		}
		render_instances[i].resource_slot_bindings[0].buffer.num_bytes = sizeof(model_instance_data_t);
		render_instances[i].resource_slot_bindings[1].type = model->mesh_resource_table ? VGPU_RESOURCE_TABLE : VGPU_RESOURCE_NONE;
		render_instances[i].resource_slot_bindings[1].resource_table = model->mesh_resource_table;
		render_instances[i].resource_slot_bindings[2].type = model->material_resource_table ? VGPU_RESOURCE_TABLE : VGPU_RESOURCE_NONE;
		render_instances[i].resource_slot_bindings[2].resource_table = model->material_resource_table;
		render_instances[i].index_buffer = model->data.index_buffer;
		render_instances[i].index_type = model->data.index_type;
		render_instances[i].instance_count = 1;
		render_instances[i].draw_offset = 0;
		render_instances[i].draw_count = model->data.draw_count;
		render_instances[i].flags = 0;
	}


	if(instance_arr)
	{
		vgpu_unlock_buffer(system->device, &lock_params);
	}

	*out_num_instances = num_instances;
	return render_instances;
}

model_t* model_system_create_model(model_system_t* system, const model_create_data_t* data)
{
	ASSERT(!system->models.full(), "too many models created");
	model_t* model = system->models.alloc();
	memcpy(&model->data, data, sizeof(model->data));

	model->mesh_resource_table = data->mesh_resource_table;
	model->material_resource_table = data->material_resource_table;
	
	model->ref_count = 0;
	return model;
}

void model_system_destroy_model(model_system_t* system, model_t* model)
{
	ASSERT(model->ref_count == 0, "Cannot destroy model with references");
	system->models.free(model);
}

model_instance_t model_system_create_instance(model_system_t* system, model_t* model, const model_instance_data_t* data)
{
	ASSERT(!system->instances.full(), "too many model instances created");
	model_instance_t instance = system->instances.alloc_handle();
	model_instance_s* instance_ptr = system->instances.handle_to_pointer(instance);
	instance_ptr->model = model;
	model->ref_count += 1;

	if ((system->gpu_caps.flags & VGPU_CAPS_FLAG_BIND_CONSTANT_BUFFER_AT_OFFSET) == 0)
	{
		vgpu_create_buffer_params_t buffer_params;
		buffer_params.num_bytes = sizeof(model_instance_data_t);
		buffer_params.usage = VGPU_USAGE_DYNAMIC;
		buffer_params.flags = VGPU_BUFFER_FLAG_CONSTANT_BUFFER;
		buffer_params.structure_stride = sizeof(model_instance_data_t);
		buffer_params.name = "model system instance buffer";
		instance_ptr->constant_buffer = vgpu_create_buffer(system->device, &buffer_params);
	}

	model_system_set_instance_data(system, instance, data);

	return instance;
}

void model_system_set_instance_data(model_system_t* system, model_instance_t instance, const model_instance_data_t* data)
{
	model_instance_s* instance_ptr = system->instances.handle_to_pointer(instance);

	if (system->gpu_caps.flags & VGPU_CAPS_FLAG_BIND_CONSTANT_BUFFER_AT_OFFSET)
		memcpy(&instance_ptr->data, data, sizeof(instance_ptr->data));
	else
		vgpu_set_buffer_data(system->device, instance_ptr->constant_buffer, 0, data, sizeof(model_instance_data_t));
}

void model_system_destroy_instance(model_system_t* system, model_instance_t instance)
{
	model_instance_s* instance_ptr = system->instances.handle_to_pointer(instance);
	
	if ((system->gpu_caps.flags & VGPU_CAPS_FLAG_BIND_CONSTANT_BUFFER_AT_OFFSET) == 0)
		vgpu_destroy_buffer(system->device, instance_ptr->constant_buffer);

	instance_ptr->model->ref_count -= 1;
	system->instances.free_handle(instance);
}
