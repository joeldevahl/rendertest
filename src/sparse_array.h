#ifndef SPARSE_ARRAY
#define SPARSE_ARRAY

template<class T, class TH = uint16_t>
struct sparse_array_t
{
	TH*          _keys;
	T*           _elements;
	size_t       _capacity;
	size_t       _length;
	allocator_t* _allocator;

	sparse_array_t() : _keys(NULL), _elements(NULL), _capacity(0), _length(0), _allocator(NULL) { }

	sparse_array_t(allocator_t* allocator, size_t capacity) : _keys(NULL), _elements(NULL), _capacity(0), _length(0), _allocator(NULL)
	{
		create(allocator, capacity);
	}

	~sparse_array_t()
	{
		if(_allocator)
		{
			ALLOCATOR_FREE(_allocator, _keys);
			ALLOCATOR_FREE(_allocator, _elements);
		}
	}

	void create(allocator_t* allocator, size_t capacity)
	{
		ASSERT(_keys == NULL, "array was already created");
		_allocator = allocator;
		_capacity = capacity;
		_length = 0;
		if(capacity)
		{
			_keys = (TH*)ALLOCATOR_ALLOC(allocator, capacity * sizeof(TH), ALIGNOF(TH));
			_elements = (T*)ALLOCATOR_ALLOC(allocator, capacity * sizeof(T), ALIGNOF(T));
		}
	}

	void grow(size_t amount = 0)
	{
		ASSERT(_allocator != NULL, "array was not created before calling set_capacity");

		_capacity = _capacity + (amount ? amount : _capacity);

		TH* new_keys = (TH*)ALLOCATOR_ALLOC(_allocator, _capacity * sizeof(TH), alignof(TH));
		T* new_elems = (T*)ALLOCATOR_ALLOC(_allocator, _capacity * sizeof(T), alignof(T));
		memcpy(new_keys, _keys, _length * sizeof(TH));
		memcpy(new_elems, _elements, _length * sizeof(T));
		ALLOCATOR_FREE(_allocator, _keys);
		ALLOCATOR_FREE(_allocator, _elements);
		_keys = new_keys;
		_elements = new_elems;
	}
		
	void set_length(size_t length)
	{
		ASSERT(length <= _capacity, "length greater than capacity");
		_length = length;
	}

	void clear()
	{
		set_length(0);
	}

	size_t capacity() const 
	{
		return _capacity;
	}

	size_t length() const
	{
		return _length;
	}

	bool empty() const 
	{
		return _length == 0;
	}

	bool any() const 
	{
		return _length != 0;
	}

	bool full() const
	{
		return _length == _capacity;
	}

	TH* begin_keys()
	{
		return _keys;
	}

	const TH* begin_keys() const
	{
		return _keys;
	}

	T* begin_elements()
	{
		return _elements;
	}

	const T* begin_elements() const
	{
		return _elements;
	}

	TH* end_keys()
	{
		return _keys + _length;
	}

	const TH* end_keys() const
	{
		return _keys + _length;
	}

	T* end_elements()
	{
		return _elements + _length;
	}

	const T* end_elements() const
	{
		return _elements + _length;
	}

	void append(const TH& key, const T& val)
	{
		ASSERT(_length < _capacity, "cannot add beyond capacity");
		size_t i = _length;
		_keys[i] = key;
		memcpy(_elements + i, &val, sizeof(T));
		_length += 1;
	}
};

#endif // SPARSE_ARRAY
