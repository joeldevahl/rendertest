#include <core/assert.h>
#include <core/hash.h>
#include <memory/allocator.h>
#include <render/render.h>
#include <renderc/types.h>
#include <resource/resource_cache.h>

#include <vgpu.h>
#include <riff/types.h>

#include "resource_context.h"
#include "render_script_creator.h"

#include <cstring>

struct render_script_resource_private_t
{
	uint32_t num_resources;
	resource_handle_t resources[];
};

static render_pipeline_set_t* create_pipeline_set(resource_context_t* resource_context, uint32_t pipeline_set_name_hash, uint32_t state_block_name_hash, vgpu_render_pass_t* render_pass)
{
	resource_cache_t* resource_cache = resource_context->resource_cache;
	vgpu_device_t* device = resource_context->device;
	render_t* render = resource_context->render;
	resource_cache_result_t res;

	resource_handle_t pipeline_set_handle;
	res = resource_cache_get_by_hash(resource_cache, pipeline_set_name_hash, &pipeline_set_handle);
	ASSERT(res == RESOURCE_CACHE_RESULT_OK);
	renderc_pipeline_set_data_t* pipeline_set_data;
	res = resource_cache_handle_handle_to_pointer(resource_cache, pipeline_set_handle, (void**)&pipeline_set_data);
	ASSERT(res == RESOURCE_CACHE_RESULT_OK);
	// TODO: listen for changes in the pipeline set

	resource_handle_t state_block_handle;
	res = resource_cache_get_by_hash(resource_cache, state_block_name_hash, &state_block_handle);
	ASSERT(res == RESOURCE_CACHE_RESULT_OK);
	renderc_state_block_data_t* state_block_data;
	res = resource_cache_handle_handle_to_pointer(resource_cache, state_block_handle, (void**)&state_block_data);
	ASSERT(res == RESOURCE_CACHE_RESULT_OK);
	// TODO: listen for changes in the state block

	const size_t MAX_PARTS = 128;
	render_pipeline_set_part_t parts[MAX_PARTS];

	for(size_t i = 0; i < pipeline_set_data->num_parts; ++i)
	{
		resource_handle_t pipeline_handle;
		res = resource_cache_get_by_hash(resource_cache, pipeline_set_data->parts[i].pipeline_name_hash, &pipeline_handle);
		ASSERT(res == RESOURCE_CACHE_RESULT_OK);
		renderc_pipeline_data_t* pipeline_data;
		res = resource_cache_handle_handle_to_pointer(resource_cache, pipeline_handle, (void**)&pipeline_data);
		ASSERT(res == RESOURCE_CACHE_RESULT_OK);
		// TODO: listen for changes in the pipeline

		resource_handle_t vp_handle;
		res = resource_cache_get_by_hash(resource_cache, pipeline_data->vp_name_hash, &vp_handle);
		ASSERT(res == RESOURCE_CACHE_RESULT_OK);
		uintptr_t vp;
		res = resource_cache_handle_handle_to_pointer(resource_cache, vp_handle, (void**)&vp);
		ASSERT(res == RESOURCE_CACHE_RESULT_OK);
		// TODO: listen to changes in the vp

		resource_handle_t fp_handle;
		res = resource_cache_get_by_hash(resource_cache, pipeline_data->fp_name_hash, &fp_handle);
		ASSERT(res == RESOURCE_CACHE_RESULT_OK);
		uintptr_t fp;
		res = resource_cache_handle_handle_to_pointer(resource_cache, fp_handle, (void**)&fp);
		ASSERT(res == RESOURCE_CACHE_RESULT_OK);
		// TODO: listen to changes in the fp

		vgpu_create_pipeline_params_t create_params = {
			resource_context->resource_layout,
			render_pass,
			(vgpu_program_t*)vp,
			(vgpu_program_t*)fp,
			*state_block_data,
			pipeline_data->prim_type,
		};

		vgpu_pipeline_t* pipeline = vgpu_create_pipeline(device, &create_params);

		resource_cache_release_handle(resource_cache, vp_handle);
		resource_cache_release_handle(resource_cache, fp_handle);
		resource_cache_release_handle(resource_cache, pipeline_handle);

		parts[i].pipeline = pipeline;
		parts[i].flags = pipeline_set_data->parts[i].flags;
	}

	render_pipeline_set_t* pipeline_set = render_create_pipeline_set(render, parts, pipeline_set_data->num_parts);

	resource_cache_release_handle(resource_cache, state_block_handle);
	resource_cache_release_handle(resource_cache, pipeline_set_handle);

	return pipeline_set;
}

static bool render_script_create(void* context, allocator_t* allocator, void* creation_data, size_t size, void** out_resource_data, void** out_private_data)
{
	resource_context_t* resource_context = (resource_context_t*)context;
	resource_cache_t* resource_cache = resource_context->resource_cache;

	uint8_t* data_raw = (uint8_t*)creation_data;
	uint8_t* data_raw_end = ((uint8_t*)creation_data) + size;

	riff_header_t* header = (riff_header_t*)creation_data;
	data_raw += sizeof(*header);
	data_raw = (uint8_t*)ALIGN_UP(data_raw, 16);

	ASSERT(riff_test_id(header, 'R', 'E', 'N', 'D'), "Wrong magic for render script");
	ASSERT(header->size >= size - sizeof(*header), "File size less then encoded size in file");

	size_t offset = 0;
	size_t total_size = 0;
	while(data_raw + offset < data_raw_end)
	{
		render_command_header_t* header = (render_command_header_t*)(data_raw + offset);

		switch(header->command)
		{
			case RENDER_COMMAND_BEGIN_PASS:
				total_size += sizeof(render_command_begin_pass_t);
				break;
			case RENDER_COMMAND_END_PASS:
				total_size += sizeof(render_command_end_pass_t);
				break;
			case RENDER_COMMAND_BLIT:
				total_size += sizeof(render_command_blit_t);
				break;
			case RENDER_COMMAND_DRAW:
				total_size += sizeof(render_command_draw_t);
				break;
			case RENDER_COMMAND_QUERY:
				{
					renderc_render_command_query_data_t* query = (renderc_render_command_query_data_t*)header;
					total_size += sizeof(renderse_command_query_t) + query->num_subsystems * sizeof(uint32_t);
				}
				break;
			default:
				ASSERT(false, "Unknown render command");
		}

		offset = ALIGN_UP(offset + header->size, 16);
	}

	render_script_t* script = (render_script_t*)ALLOCATOR_ALLOC(allocator, sizeof(render_script_t), 16);
	script->commands_size = total_size;
	script->commands = ALLOCATOR_ALLOC(allocator, total_size, 16);

	uint8_t* ptr = (uint8_t*)script->commands;
	offset = 0;
	vgpu_render_pass_t* current_render_pass = nullptr;
	while(data_raw + offset < data_raw_end)
	{
		render_command_header_t* header = (render_command_header_t*)(data_raw + offset);

		size_t write_size = 0;
		switch(header->command)
		{
			case RENDER_COMMAND_BEGIN_PASS:
				{
					renderc_render_command_begin_pass_data_t* src = (renderc_render_command_begin_pass_data_t*)header;
					render_command_begin_pass_t* dst = (render_command_begin_pass_t*)ptr;
					dst->header.command = RENDER_COMMAND_BEGIN_PASS;
					dst->header.size = sizeof(*dst);

					resource_handle_t render_pass_handle;
					resource_cache_result_t res = resource_cache_get_by_hash(resource_cache, src->render_pass_name_hash, &render_pass_handle);
					ASSERT(res == RESOURCE_CACHE_RESULT_OK);
					res = resource_cache_handle_handle_to_pointer(resource_cache, render_pass_handle, (void**)&dst->render_pass);
					ASSERT(res == RESOURCE_CACHE_RESULT_OK);
					// TODO: listen for changes in the render pass
					// TODO: store handle to be released later

					current_render_pass = dst->render_pass;

					write_size = dst->header.size;
				}
				break;
			case RENDER_COMMAND_END_PASS:
				{
					renderc_render_command_end_pass_data_t* src = (renderc_render_command_end_pass_data_t*)header;
					render_command_end_pass_t* dst = (render_command_end_pass_t*)ptr;
					dst->header.command = RENDER_COMMAND_END_PASS;
					dst->header.size = sizeof(*dst);
					write_size = dst->header.size;
					break;
				}
			case RENDER_COMMAND_BLIT:
			case RENDER_COMMAND_DRAW:
				break;
			case RENDER_COMMAND_QUERY:
				{
					renderc_render_command_query_data_t* src = (renderc_render_command_query_data_t*)header;

					renderse_command_query_t* dst = (renderse_command_query_t*)ptr;
					dst->header.command = RENDER_COMMAND_QUERY;
					dst->header.size = sizeof(*dst) + src->num_subsystems*sizeof(uint32_t);
					dst->pipeline_set = create_pipeline_set(resource_context, src->pipeline_set_name_hash, src->state_block_name_hash, current_render_pass);
					dst->global_resource_table = nullptr;
					dst->view_context_name_hash = src->view_context_name_hash;
					dst->num_subsystems = src->num_subsystems;
					for(size_t i = 0; i < src->num_subsystems; ++i)
						dst->subsystem_name_hashes[i] = src->subsystem_name_hashes[i];
					dst->and_mask = src->and_mask;
					dst->not_mask = src->not_mask;
					write_size = dst->header.size;
				}
				break;
			default:
				ASSERT(false, "Unknown render command");
		}

		offset = ALIGN_UP(offset + header->size, 16);
		ptr = (uint8_t*)ALIGN_UP(ptr + write_size, 16);
	}

	*out_resource_data = (void*)script;

	return true;
}

static bool render_script_recreate(void* context, allocator_t* allocator, void* creation_data, size_t size, void* prev_resource_data, void* prev_private_data, void** out_resource_data, void** out_private_data)
{
	return false;
}

static void render_script_destroy(void* context, allocator_t* allocator, void* resource_data, void* private_data)
{
	resource_context_t* resource_context = (resource_context_t*) context;
	render_t* render = resource_context->render;

	render_script_t* script = (render_script_t*)resource_data;

	uint8_t* ptr = (uint8_t*)script->commands;
	uint8_t* end = ptr + script->commands_size;
	while(ptr < end)
	{
		render_command_header_t* header = (render_command_header_t*)ptr;
		switch(header->command)
		{
			case RENDER_COMMAND_BEGIN_PASS:
			case RENDER_COMMAND_END_PASS:
			case RENDER_COMMAND_BLIT:
			case RENDER_COMMAND_DRAW:
				break;
			case RENDER_COMMAND_QUERY:
				{
					renderse_command_query_t* query = (renderse_command_query_t*)header;
					render_destroy_pipeline_set(render, query->pipeline_set);
				}
				break;
			default:
				break;
		}
		ptr += ALIGN_UP(header->size, 16);
	}
	ALLOCATOR_FREE(allocator, script->commands);
	ALLOCATOR_FREE(allocator, script);
}

void render_script_register_creator(resource_context_t* resource_context)
{
	resource_creator_t render_script_creator = {
		render_script_create,
		render_script_recreate,
		render_script_destroy,
		&allocator_default,
		resource_context,
		hash_string("render_script")
	};
	resource_cache_add_creator(resource_context->resource_cache, &render_script_creator);
}
