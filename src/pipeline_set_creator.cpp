#include <core/assert.h>
#include <core/hash.h>
#include <memory/allocator.h>
#include <render/render.h>
#include <resource/resource_cache.h>

#include <riff/types.h>

#include "resource_context.h"

#include <cstring>

struct pipeline_set_resource_private_t
{
	uint32_t num_resources;
	resource_handle_t resources[];
};

static bool pipeline_set_create(void* context, allocator_t* allocator, void* creation_data, size_t size, void** out_resource_data, void** out_private_data)
{
	resource_context_t* resource_context = (resource_context_t*)context;
	render_t* render = resource_context->render;
	resource_cache_t* resource_cache = resource_context->resource_cache;

	uint8_t* data_raw = (uint8_t*)creation_data;

	riff_header_t* header = (riff_header_t*)creation_data;
	data_raw += sizeof(*header);

	ASSERT(riff_test_id(header, 'P', 'S', 'E', 'T'), "Wrong magic for pipeline set");
	ASSERT(header->size >= size - sizeof(*header), "File size less then encoded size in file");

	*out_resource_data = ALLOCATOR_ALLOC(allocator, header->size, 16);
	memcpy(*out_resource_data, data_raw, header->size);

	return true;
}

static bool pipeline_set_recreate(void* context, allocator_t* allocator, void* creation_data, size_t size, void* prev_resource_data, void* prev_private_data, void** out_resource_data, void** out_private_data)
{
	return false;
}

static void pipeline_set_destroy(void* context, allocator_t* allocator, void* resource_data, void* private_data)
{
	ALLOCATOR_FREE(allocator, resource_data);

	/*
	resource_context_t* resource_context = (resource_context_t*)context;
	render_t* render = resource_context->render;
	resource_cache_t* resource_cache = resource_context->resource_cache;

	render_destroy_pipeline_set(render, (render_pipeline_set_t)resource_data);

	resource_cache_result_t res;
	pipeline_set_resource_private_t* pdata = (pipeline_set_resource_private_t*)private_data;
	for(size_t i = 0; i < pdata->num_resources; ++i)
	{
		res = resource_cache_release_handle(resource_cache, pdata->resources[i]);
		ASSERT(res == RESOURCE_CACHE_RESULT_OK);
	}
	ALLOCATOR_FREE(allocator, pdata);
	*/
}

void pipeline_set_register_creator(resource_context_t* resource_context)
{
	resource_creator_t pipeline_set_creator = {
		pipeline_set_create,
		pipeline_set_recreate,
		pipeline_set_destroy,
		&allocator_default,
		resource_context,
		hash_string("pipeline_set")
	};
	resource_cache_add_creator(resource_context->resource_cache, &pipeline_set_creator);
}
