#include "render_script_executor.h"

#include <core/hash.h>
#include <core/assert.h>
#include <core/time.h>
#include <memory/memory.h>
#include <render/render.h>

#include <vgpu.h>

#pragma warning( disable : 4530 ) // No exception handler enabled
#include <unordered_map>

struct render_script_executor_t
{
	allocator_t* allocator;
	allocator_t* command_heap;
	render_t* render;

	vgpu_device_t* device;
	vgpu_thread_context_t* thread_context;
	vgpu_command_list_t* command_list;
	vgpu_texture_t* backbuffer_tex;
	vgpu_texture_t* depth_stencil_tex;
	vgpu_render_pass_t* backbuffer_rs;

	std::unordered_map<uint32_t, render_script_subsystem_t> subsystems;
	std::unordered_map<uint32_t, render_script_view_context_t> view_contexts;
};

render_script_executor_t* render_script_executor_create(allocator_t* allocator, render_t* render, vgpu_device_t* device, vgpu_thread_context_t* thread_context, vgpu_command_list_t* command_list)
{
	render_script_executor_t* executor = ALLOCATOR_NEW(allocator, render_script_executor_t);
	executor->allocator = allocator;
	executor->command_heap = allocator_incheap_create(&allocator_default, 1024 * 1024);
	executor->render = render;
	executor->device = device;
	executor->thread_context = thread_context;
	executor->command_list = command_list;

	vgpu_create_texture_params_t ds_params;
	ds_params.type = VGPU_TEXTURETYPE_2D;
	ds_params.format = VGPU_TEXTUREFORMAT_D32F_S8X24;
	ds_params.usage = VGPU_USAGE_DEFAULT;
	ds_params.width = 1280;
	ds_params.height = 720;
	ds_params.depth = 1;
	ds_params.num_mips = 1;
	ds_params.is_render_target = true;
	ds_params.name = "depth buffer";
	ds_params.clear_value.depth_stencil.depth = 1.0f;
	ds_params.clear_value.depth_stencil.stencil = 0;
	executor->depth_stencil_tex = vgpu_create_texture(device, &ds_params);

	executor->backbuffer_tex = vgpu_get_back_buffer(device);

	vgpu_create_render_pass_params_t rs_params;
	rs_params.num_color_targets = 1;
	rs_params.color_targets[0].texture = executor->backbuffer_tex;
	rs_params.color_targets[0].clear_on_bind = true;
	rs_params.depth_stencil_target.texture = executor->depth_stencil_tex;
	rs_params.depth_stencil_target.clear_on_bind = true;
	executor->backbuffer_rs = vgpu_create_render_pass(device, &rs_params);

	return executor;
}

void render_script_executor_destroy(render_script_executor_t* executor)
{
	vgpu_destroy_render_pass(executor->device, executor->backbuffer_rs);
	vgpu_destroy_texture(executor->device, executor->depth_stencil_tex);
	allocator_incheap_destroy(executor->command_heap);
	ALLOCATOR_DELETE(executor->allocator, render_script_executor_t, executor);
}

void render_script_executor_add_subsystem(render_script_executor_t* executor, const char* name, const render_script_subsystem_t* subsystem)
{
	executor->subsystems[hash_string(name)] = *subsystem;
}

void render_script_executor_add_view_context(render_script_executor_t* executor, const char* name, const render_script_view_context_t* view_context)
{
	executor->view_contexts[hash_string(name)] = *view_context;
}

void render_script_executor_run_script(render_script_executor_t* executor, render_script_t* script)
{
	uint8_t* data = (uint8_t*)script->commands;
	size_t offset = 0;
	size_t frame_index = vgpu_get_frame_no(executor->device) % VGPU_MULTI_BUFFERING;

	while(offset < script->commands_size)
	{
		size_t command_size = 0;
		render_command_header_t* header = (render_command_header_t*)(data + offset);

		switch(header->command)
		{
			case RENDER_COMMAND_BEGIN_PASS:
			case RENDER_COMMAND_END_PASS:
				{
					void* dst = ALLOCATOR_ALLOC(executor->command_heap, header->size, 16);
					memcpy(dst, header, header->size);
				}
				break;
			case RENDER_COMMAND_QUERY:
				{
					renderse_command_query_t* src = (renderse_command_query_t*)header;
					allocator_helper_t helper = allocator_helper_create(executor->command_heap);
					ALLOCATOR_HELPER_ADD(&helper, 1, render_command_draw_t);
					ALLOCATOR_HELPER_ADD(&helper, src->num_subsystems, render_instance_t*);
					ALLOCATOR_HELPER_ADD(&helper, src->num_subsystems, size_t);
					allocator_helper_commit(&helper);
					render_command_query_t* query = ALLOCATOR_HELPER_GET(&helper, 1, render_command_query_t);
					render_instance_t** instances = ALLOCATOR_HELPER_GET(&helper, src->num_subsystems, render_instance_t*);
					size_t* num_instances = ALLOCATOR_HELPER_GET(&helper, src->num_subsystems, size_t);

					render_script_view_context_t* vc = &executor->view_contexts[src->view_context_name_hash];

					// TODO: only query subsystems once per frame
					for(size_t i = 0; i < src->num_subsystems; ++i)
					{
						render_script_subsystem_t* ss = &executor->subsystems[src->subsystem_name_hashes[i]];
						render_script_query_context_t query_context = {
							vc,
						};
						instances[i] = ss->query_func(ss->user_pointer, &query_context, &num_instances[i]);
					}

					query->header.command = RENDER_COMMAND_QUERY;
					query->header.size = ALIGN_UP(allocator_helper_size(&helper), 16);
					query->pipeline_set = src->pipeline_set;
					query->global_resource_table = vc->resource_table;
					query->num_instance_lists = src->num_subsystems;
					query->instance_list_lengths = num_instances;
					query->instance_lists = instances;
					query->and_mask = src->and_mask;
					query->not_mask = src->not_mask;
				}
				break;
			case RENDER_COMMAND_DRAW:
			case RENDER_COMMAND_BLIT:
			default:
				ASSERT(false, "Unknown render command");
		}

		offset = ALIGN_UP(offset + header->size, 16);
	}

	const void* cmd_start = (const void*)ALIGN_UP(allocator_incheap_start(executor->command_heap), 16);
	const void* cmd_end = allocator_incheap_curr(executor->command_heap);
	render_process_commands(executor->render, cmd_start, cmd_end);
	allocator_incheap_reset(executor->command_heap);
}
