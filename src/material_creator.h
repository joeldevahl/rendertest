#pragma once

#include <render/types.h>

struct material_s
{
	vgpu_resource_table_t* resource_table;
	vgpu_buffer_t* constant_buffer;
};

void material_register_creator(struct resource_context_t* resource_context);
