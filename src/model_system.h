#ifndef MODEL_SYSTEM_H
#define MODEL_SYSTEM_H

#include <core/types.h>
#include <memory/allocator.h>
#include <vgpu.h>

#include "render_script_executor.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct model_system_s model_system_t;
typedef struct model_s model_t;
typedef uint32_t model_instance_t;

typedef struct model_system_create_params_s
{
	allocator_t* allocator;
	vgpu_device_t* device;
	vgpu_resource_layout_t* resource_layout;
	size_t max_models;
	size_t max_model_instances;
} model_system_create_params_t;

typedef struct model_create_data_s
{
	vgpu_buffer_t* index_buffer;
	vgpu_data_type_t index_type;
	size_t draw_count;
	vgpu_resource_table_t* mesh_resource_table;
	vgpu_resource_table_t* material_resource_table;
} model_create_data_t;

typedef struct model_instance_data_s
{
	float transform[16];
	float dummy[4 * 12];
} model_instance_data_t;

model_system_t* model_system_create(const model_system_create_params_t* params);

void model_system_destroy(model_system_t* system);

render_instance_t* model_system_query(void* user_pointer, const render_script_query_context_t* context, size_t* out_num_instances);

model_t* model_system_create_model(model_system_t* system, const model_create_data_t* data);

void model_system_destroy_model(model_system_t* system, model_t* model);

model_instance_t model_system_create_instance(model_system_t* system, model_t* model, const model_instance_data_t* data);

void model_system_set_instance_data(model_system_t* system, model_instance_t instance, const model_instance_data_t* data);

void model_system_destroy_instance(model_system_t* system, model_instance_t instance);

#ifdef __cplusplus
}
#endif

#endif // MODEL_SYSTEM_H
