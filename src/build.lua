Unit:Using("core")
Unit:Using("io")
Unit:Using("application")
Unit:Using("render")
Unit:Using("container")
Unit:Using("math")
Unit:Using("resource")
Unit:Using("dl")

Unit:UsingHeaders("vertex_layout")
Unit:UsingHeaders("mesh")
Unit:UsingHeaders("riff")
Unit:UsingHeaders("renderc")

function Unit.Init(self)
	self.executable = true
	self.targetname = "rendertest"
end

function Unit.Build(self)
	local src = Collect(self.path .. "/*.cpp")
	local obj = Compile(self.settings, src)
	local bin = Link(self.settings, self.targetname, obj)
	self:AddProduct(bin)
end
