#include <core/assert.h>
#include <core/hash.h>
#include <memory/allocator.h>
#include <vgpu.h>
#include <resource/resource_cache.h>

#include <units/material/types/material.h>

#include "material_creator.h"
#include "resource_context.h"

#include <dl/dl.h>

#include <units/material/types/material.h>

static const unsigned char material_typelib[] =
{
#include <units/material/types/material.tlb.hex>
};

static bool material_create(void* context, allocator_t* allocator, void* creation_data, size_t size, void** out_resource_data, void** out_private_data)
{
	resource_context_t* resource_context = (resource_context_t*)context;
	vgpu_device_t* device = resource_context->device;
	material_s* material = ALLOCATOR_NEW(allocator, material_s);

	material_data_t* material_data = nullptr;
	size_t consumed = 0;
	dl_error_t err = dl_instance_load_inplace(
			resource_context->dl_ctx,
			material_data_t::TYPE_ID,
			(unsigned char*)creation_data,
			size,
			(void**)&material_data,
			&consumed);
	ASSERT(err == DL_ERROR_OK);

	vgpu_create_buffer_params_t constant_buffer_params;
	constant_buffer_params.num_bytes = ALIGN_UP(sizeof(*material_data), 256);
	constant_buffer_params.usage = VGPU_USAGE_DEFAULT;
	constant_buffer_params.flags = VGPU_BUFFER_FLAG_CONSTANT_BUFFER;
	constant_buffer_params.structure_stride = 0;
	constant_buffer_params.name = "cb";
	material->constant_buffer = vgpu_create_buffer(device, &constant_buffer_params);
	vgpu_set_buffer_data(resource_context->device, material->constant_buffer, 0, material_data, sizeof(*material_data));

	vgpu_resource_table_entry_t cb_entry = {
		2,
		VGPU_RESOURCE_BUFFER,
		material->constant_buffer,
		0,
		constant_buffer_params.num_bytes,
		true
	};
	material->resource_table = vgpu_create_resource_table(device, resource_context->resource_layout, 2, &cb_entry, 1);

	*out_resource_data = (void*)material;
	return true;
}

static bool material_recreate(void* context, allocator_t* allocator, void* creation_data, size_t size, void* prev_resource_data, void* prev_private_data, void** out_resource_data, void** out_private_data)
{
	return false;
}

static void material_destroy(void* context, allocator_t* allocator, void* resource_data, void* private_data)
{
	resource_context_t* resource_context = (resource_context_t*)context;
	vgpu_device_t* device = resource_context->device;
	material_s* material = (material_s*)resource_data;

	vgpu_destroy_resource_table(device, material->resource_table);
	vgpu_destroy_buffer(device, material->constant_buffer);

	ALLOCATOR_DELETE(allocator, material_s, material);
}

void material_register_creator(resource_context_t* resource_context)
{
	dl_context_load_type_library(resource_context->dl_ctx, material_typelib, ARRAY_LENGTH(material_typelib));

	resource_creator_t material_creator = {
		material_create,
		material_recreate,
		material_destroy,
		&allocator_default,
		resource_context,
		hash_string("material")
	};
	resource_cache_add_creator(resource_context->resource_cache, &material_creator);
}
