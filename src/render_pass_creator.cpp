#include <core/assert.h>
#include <core/hash.h>
#include <memory/allocator.h>
#include <resource/resource_cache.h>

#include <vgpu.h>
#include <renderc/types.h>

#include "render_target_creator.h"

static bool render_pass_create(void* context, allocator_t* allocator, void* creation_data, size_t size, void** out_resource_data, void** out_private_data)
{
	resource_context_t* resource_context = (resource_context_t*)context;
	resource_cache_t* resource_cache = resource_context->resource_cache;
	vgpu_device_t* device = resource_context->device;

	renderc_render_pass_data_t* data = (renderc_render_pass_data_t*)creation_data;

	vgpu_create_render_pass_params_t params = {};
	params.num_color_targets = data->num_color_parts;
	uint32_t backbuffer_hash = hash_string("backbuffer");
	for (size_t i = 0; i < data->num_color_parts; ++i)
	{
		if (data->color_parts[i].render_target_name_hash == backbuffer_hash)
		{
			params.color_targets[i].texture = vgpu_get_back_buffer(device);
		}
		else
		{
			resource_handle_t render_target_handle;
			resource_cache_result_t res = resource_cache_get_by_hash(resource_cache, data->color_parts[i].render_target_name_hash, &render_target_handle);
			ASSERT(res == RESOURCE_CACHE_RESULT_OK);
			res = resource_cache_handle_handle_to_pointer(resource_cache, render_target_handle, (void**)&params.color_targets[i].texture);
			ASSERT(res == RESOURCE_CACHE_RESULT_OK);
			// TODO: listen for changes in the render target
			// TODO: store handle to be released later
		}
		params.color_targets[i].clear_on_bind = true; // TODO: get this from data
	}

	if (data->depth_stencil_part.render_target_name_hash != 0)
	{
		resource_handle_t render_target_handle;
		resource_cache_result_t res = resource_cache_get_by_hash(resource_cache, data->depth_stencil_part.render_target_name_hash, &render_target_handle);
		ASSERT(res == RESOURCE_CACHE_RESULT_OK);
		res = resource_cache_handle_handle_to_pointer(resource_cache, render_target_handle, (void**)&params.depth_stencil_target.texture);
		ASSERT(res == RESOURCE_CACHE_RESULT_OK);
		// TODO: listen for changes in the render target
		// TODO: store handle to be released later

		params.depth_stencil_target.clear_on_bind = true; // TODO: get this from data
	}

	vgpu_render_pass_t* render_pass = vgpu_create_render_pass(device, &params);

	*out_resource_data = render_pass;

	return true;
}

static bool render_pass_recreate(void* context, allocator_t* allocator, void* creation_data, size_t size, void* prev_resource_data, void* prev_private_data, void** out_resource_data, void** out_private_data)
{
	return false;
}

static void render_pass_destroy(void* context, allocator_t* allocator, void* resource_data, void* private_data)
{
	resource_context_t* resource_context = (resource_context_t*)context;
	vgpu_device_t* device = resource_context->device;
	vgpu_render_pass_t* render_pass = (vgpu_render_pass_t*)resource_data;
	vgpu_destroy_render_pass(device, render_pass);
}

void render_pass_register_creator(resource_context_t* resource_context)
{
	resource_creator_t render_pass_creator = {
		render_pass_create,
		render_pass_recreate,
		render_pass_destroy,
		&allocator_default,
		resource_context,
		hash_string("render_pass")
	};
	resource_cache_add_creator(resource_context->resource_cache, &render_pass_creator);
}
