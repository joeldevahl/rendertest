Unit:Using("io")
Unit:Using("sexp")
Unit:UsingHeaders("vgpu")
Unit:Using("vertex_layout")
Unit:UsingHeaders("riff")
Unit:Using("assimp")
Unit:Using("dl")
Unit:Using("getopt")

function Unit.Init(self)
	self.executable = true
	--self.static_library = true
	self.targetname = "mesh"
	self.ignore_application_lib = true
end

function Unit.AddTools(self)
	AddTool(function (settings)
		settings.meshc = {}
		settings.meshc.exe = GetHostBinary("meshc")

		settings.compile.mappings["mesh"] = function (settings, infile)
			local outfile = PathJoin(target.outdir, infile)
			local meshc = settings.meshc
			local exe = meshc.exe
			local cmd = exe .. " -o " .. outfile .. " " .. infile

			AddJob(outfile, "mesh " .. infile, cmd)
			AddDependency(outfile, meshc.exe)
			AddDependency(outfile, infile)

			for line in io.lines(infile) do
				local dep_type, file = string.match(line, "//%s*dep%s+(.+)%s+(.+)")
				if dep_type and file then
					if dep_type == "source" then
						AddDependency(outfile, file)
					else
						-- TODO: transform source to binary version
					end
				end
			end

			return outfile
		end
	end)
end

function Unit.BuildTarget(self)
	local typelib_src = Collect(self.path .. "/types/*.tld")
	local typelib = Compile(self.settings, typelib_src);
	self:AddProduct(typelib)
end

function Unit.Build(self)
		local bin_obj = Compile(self.settings, PathJoin(self.path, "src/meshc.cpp"))
		local bin = Link(self.settings, self.targetname .. "c", bin_obj)
		self:AddProduct(bin)
end
