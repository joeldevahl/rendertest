Unit:Using("dl")

function Unit.Init(self)
	DefaultInit(self)
	self.static_library = false
end

function Unit.AddTools(self)
	AddTool(function (settings)
		settings.materialc = {}
		settings.materialc.libname = "units/material/types/material.tlb"

		settings.compile.mappings["material"] = function (settings, infile)
			local outfile = PathJoin(target.outdir, PathBase(infile)) .. ".material"
			local libname = PathJoin(target.outdir, settings.materialc.libname)
			return DLPack(settings, infile, outfile, libname)
		end
	end)
end

function Unit.BuildTarget(self)
	local typelib_src = Collect(self.path .. "/types/*.tld")
	local typelib = Compile(self.settings, typelib_src);
	self:AddProduct(typelib)
end
