#include <core/assert.h>
#include <core/hash.h>
#include <memory/allocator.h>
#include <memory/memory.h>
#include <container/array.h>
#include <io/file.h>
#include <sexp/sexp.h>

#define VGPU_TRANSLATION_UTILS
#include <vgpu.h>

#include <riff/types.h>
#include <renderc/types.h>

#include <getopt/getopt.h>

#include <cstring>
#include <cstdio>

#if defined(FAMILY_WINDOWS)
#	include <core/windows/windows.h>
#endif

#define ERROR_AND_QUIT(fmt, ...) { fprintf(stderr, "Error: " fmt "\n", ##__VA_ARGS__); return 0x0; }
#define ERROR_AND_FAIL(fmt, ...) { fprintf(stderr, "Error: " fmt "\n", ##__VA_ARGS__); return 1; }
#define ERROR_AND_FAIL_EXIT(fmt, ...) { fprintf(stderr, "Error: " fmt "\n", ##__VA_ARGS__); exit(1); }

const char* sexptos(sexp_t* exp)
{
	switch(exp->type)
	{
		case SEXP_TYPE_STRING:
			return exp->string;
		case SEXP_TYPE_SYMBOL:
			return exp->symbol;
		case SEXP_TYPE_QUOTE:
			return sexptos(exp->quote);
		default:
			return NULL;
	}
}

sexp_t* tagged_list(sexp_t* list, const char* name)
{
	sexp_t* data = sexp_car(list);
	if(strcmp(sexptos(data), name) == 0)
		return sexp_cdr(list);
	return NULL;
}

sexp_t* assoc(sexp_t* list, const char* name)
{
	while(!sexp_is_nil(list))
	{
		sexp_t* sublist = sexp_car(list);
		sexp_t* data = sexp_car(sublist);
		if(strcmp(sexptos(data), name) == 0)
			return sexp_cdr(sublist);
		list = sexp_cdr(list);
	}

	return NULL;
}

#define first(exp) sexp_car(exp)
#define rest(exp) sexp_cdr(exp)
#define second(exp) sexp_car(sexp_cdr(exp))
#define third(exp) sexp_car(sexp_cdr(sexp_cdr(exp)))
#define forth(exp) sexp_car(sexp_cdr(sexp_cdr(sexp_cdr(exp))))

void parse_blend(vgpu_blend_t* blend, sexp_t* list)
{
	while(!sexp_is_nil(list))
	{
		sexp_t* elem = sexp_car(list);
		sexp_t* data = sexp_car(elem);

		if(strcmp(data->string, "enabled") == 0)
		{
			elem = sexp_cdr(elem);
			data = sexp_car(elem);
			blend->enabled = data->boolean;
		}

		list = sexp_cdr(list);
	}
}

void parse_depth(vgpu_depth_t* depth, sexp_t* list)
{
	while(!sexp_is_nil(list))
	{
		sexp_t* elem = sexp_car(list);
		sexp_t* data = sexp_car(elem);

		const char* str = sexptos(data);
		if(strcmp(str, "enabled") == 0)
		{
			elem = sexp_cdr(elem);
			data = sexp_car(elem);
			depth->enabled = data->boolean;
		}

		list = sexp_cdr(list);
	}
}

void parse_stencil(vgpu_stencil_t* stencil, sexp_t* list)
{
	while(!sexp_is_nil(list))
	{
		sexp_t* elem = sexp_car(list);
		sexp_t* data = sexp_car(elem);

		const char* str = sexptos(data);
		if(strcmp(str, "enabled") == 0)
		{
			elem = sexp_cdr(elem);
			data = sexp_car(elem);
			stencil->enabled = data->boolean;
		}

		list = sexp_cdr(list);
	}
}

int compile_state_block(const char* outfilename, const char* infilename)
{
	char* indata = file_open_read_all_text(infilename, &allocator_default);
	sexp_t* root = sexp_read(&allocator_default, indata);
	ALLOCATOR_FREE(&allocator_default, indata);

	renderc_state_block_data_t outdata;
	memory_zero(&outdata, sizeof(outdata));

	sexp_t* list = sexp_cdr(root);
	while(!sexp_is_nil(list))
	{
		sexp_t* elem = sexp_car(list);
		sexp_t* data = sexp_car(elem);

		const char* str = sexptos(data);
		if(strcmp(str, "fill") == 0)
		{
			elem = sexp_cdr(elem);
			data = sexp_car(elem);
			outdata.fill = vgpu_fill_mode_from_string(sexptos(data));
		}
		else if(strcmp(str, "wind") == 0)
		{
			elem = sexp_cdr(elem);
			data = sexp_car(elem);
			outdata.wind = vgpu_winding_order_from_string(sexptos(data));
		}
		else if(strcmp(str, "cull") == 0)
		{
			elem = sexp_cdr(elem);
			data = sexp_car(elem);
			outdata.cull = vgpu_cull_mode_from_string(sexptos(data));
		}
		else if(strcmp(str, "depth-bias") == 0)
		{
			elem = sexp_cdr(elem);
			data = sexp_car(elem);
			outdata.depth_bias = data->fixnum;
		}
		else if(strcmp(str, "blend-independent") == 0)
		{
			elem = sexp_cdr(elem);
			data = sexp_car(elem);
			outdata.blend_independent = data->boolean;
		}
		else if(strcmp(str, "blend") == 0)
		{
			elem = sexp_cdr(elem);
			if(outdata.blend_independent)
			{
				for(int i = 1; i < VGPU_MAX_RENDER_TARGETS; ++i)
				{
					data = sexp_car(elem);
					parse_blend(&outdata.blend[i], data);
					elem = sexp_cdr(elem);
				}
			}
			else
			{
				parse_blend(&outdata.blend[0], elem);
				for(int i = 1; i < VGPU_MAX_RENDER_TARGETS; ++i)
				{
					memory_copy(&outdata.blend[i], &outdata.blend[0], sizeof(outdata.blend[0]));
				}
			}
		}
		else if(strcmp(str, "depth") == 0)
		{
			elem = sexp_cdr(elem);
			parse_depth(&outdata.depth, elem);
		}
		else if(strcmp(str, "stencil") == 0)
		{
			elem = sexp_cdr(elem);
			parse_stencil(&outdata.stencil, elem);
		}

		list = sexp_cdr(list);
	}

	file_t* outfile = file_open(outfilename, FILE_MODE_WRITE);
	if(outfile == NULL)
		return -1;

	riff_header_t header = {
		{ 'S', 'T', 'A', 'T' },
		sizeof(outdata)
	};

	file_write(outfile, &header, sizeof(riff_header_t));
	file_write(outfile, &outdata, sizeof(outdata));
	file_close(outfile);

	return 0;
}

int compile_render_target(const char* outfilename, const char* infilename)
{
	char* indata = file_open_read_all_text(infilename, &allocator_default);
	sexp_t* root = sexp_read(&allocator_default, indata);
	ALLOCATOR_FREE(&allocator_default, indata);

	renderc_render_target_data_t outdata = {};

	sexp_t* list = sexp_cdr(root);
	if (sexp_t* elem = assoc(list, "flags"))
		outdata.flags = first(elem)->fixnum;
	if (sexp_t* elem = assoc(list, "width"))
		outdata.width = first(elem)->fixnum;
	if (sexp_t* elem = assoc(list, "height"))
		outdata.height = first(elem)->fixnum;
	if (sexp_t* elem = assoc(list, "format"))
		outdata.format = first(elem)->fixnum;

	file_t* outfile = file_open(outfilename, FILE_MODE_WRITE);
	if (outfile == NULL)
		return -1;

	file_write(outfile, &outdata, sizeof(outdata));
	file_close(outfile);

	return 0;
}

int compile_render_pass(const char* outfilename, const char* infilename)
{
	char* indata = file_open_read_all_text(infilename, &allocator_default);
	sexp_t* root = sexp_read(&allocator_default, indata);
	ALLOCATOR_FREE(&allocator_default, indata);

	renderc_render_pass_data_t outdata = {};

	sexp_t* list = rest(root);
	if (sexp_t* depth_stencil = assoc(list, "depth-stencil"))
	{
		outdata.depth_stencil_part.render_target_name_hash = hash_string(sexptos(first(depth_stencil)));
	}

	outdata.num_color_parts = 0;
	if (sexp_t* color_list = assoc(list, "color"))
	{
		while (!sexp_is_nil(color_list))
		{
			outdata.color_parts[outdata.num_color_parts].render_target_name_hash = hash_string(sexptos(first(color_list)));
			color_list = rest(color_list);
			++outdata.num_color_parts;
		}
	}

	file_t* outfile = file_open(outfilename, FILE_MODE_WRITE);
	if (outfile == NULL)
		return -1;

	file_write(outfile, &outdata, sizeof(outdata));
	file_close(outfile);

	return 0;
}

int compile_pipeline(const char* outfilename, const char* infilename)
{
	char* indata = file_open_read_all_text(infilename, &allocator_default);
	sexp_t* root = sexp_read(&allocator_default, indata);
	ALLOCATOR_FREE(&allocator_default, indata);

	renderc_pipeline_data_t outdata;

	sexp_t* list = sexp_cdr(root);
	if(sexp_t* elem = assoc(list, "vp"))
		outdata.vp_name_hash = hash_string(sexptos(first(elem)));

	if(sexp_t* elem = assoc(list, "fp"))
		outdata.fp_name_hash = hash_string(sexptos(first(elem)));

	if(sexp_t* elem = assoc(list, "primitive"))
		outdata.prim_type = vgpu_primitive_type_from_string(sexptos(first(elem)));

	file_t* outfile = file_open(outfilename, FILE_MODE_WRITE);
	if(outfile == NULL)
		return -1;

	riff_header_t header = {
		{ 'P', 'I', 'P', 'E' },
		sizeof(outdata)
	};

	file_write(outfile, &header, sizeof(riff_header_t));
	file_write(outfile, &outdata, sizeof(outdata));
	file_close(outfile);

	return 0;
}

int compile_pipeline_set(const char* outfilename, const char* infilename)
{
	char* indata = file_open_read_all_text(infilename, &allocator_default);
	sexp_t* root = sexp_read(&allocator_default, indata);
	ALLOCATOR_FREE(&allocator_default, indata);

	const size_t MAX_PIPELINE_SET_PARTS = 128;
	renderc_pipeline_set_part_data_t parts[MAX_PIPELINE_SET_PARTS];
	size_t num_parts = 0;

	sexp_t* list = sexp_cdr(root);
	while(!sexp_is_nil(list))
	{
		sexp_t* elem = sexp_car(list);
		sexp_t* data = sexp_car(elem);
		parts[num_parts].pipeline_name_hash = hash_string(sexptos(data));

		elem = sexp_cdr(elem);
		data = sexp_car(elem);
		parts[num_parts].flags = data->fixnum;

		++num_parts;
		list = sexp_cdr(list);
	}

	size_t part_size = num_parts * sizeof(parts[0]);
	size_t total_size = sizeof(renderc_pipeline_set_data_t) + part_size;
	renderc_pipeline_set_data_t* outdata = (renderc_pipeline_set_data_t*)ALLOCATOR_ALLOC(&allocator_default, total_size, 16);
	outdata->num_parts = num_parts;
	memcpy(outdata->parts, parts, part_size);

	file_t* outfile = file_open(outfilename, FILE_MODE_WRITE);
	if(outfile == NULL)
		return -1;

	riff_header_t header = {
		{ 'P', 'S', 'E', 'T' },
		(uint32_t)total_size
	};

	file_write(outfile, &header, sizeof(riff_header_t));
	file_write(outfile, outdata, total_size);
	file_close(outfile);

	return 0;
}

int compile_render_script(const char* outfilename, const char* infilename)
{
	char* indata = file_open_read_all_text(infilename, &allocator_default);
	sexp_t* root = sexp_read(&allocator_default, indata);
	ALLOCATOR_FREE(&allocator_default, indata);

	allocator_t* incheap = allocator_incheap_create(&allocator_default, 1024 * 1024);

	sexp_t* list = sexp_cdr(root);

	if(sexp_t* commands = assoc(list, "commands"))
	{
		while(!sexp_is_nil(commands))
		{
			sexp_t* command = first(commands);
			if (sexp_t* elem = tagged_list(command, "begin-pass"))
			{
				renderc_render_command_begin_pass_data_t* pass = ALLOCATOR_ALLOC_TYPE_ALIGNED(incheap, renderc_render_command_begin_pass_data_t, 16);
				pass->header.command = RENDER_COMMAND_BEGIN_PASS;
				pass->header.size = sizeof(*pass);
				pass->render_pass_name_hash = hash_string(sexptos(first(elem)));
			}
			if (sexp_t* elem = tagged_list(command, "end-pass"))
			{
				renderc_render_command_end_pass_data_t* pass = ALLOCATOR_ALLOC_TYPE_ALIGNED(incheap, renderc_render_command_end_pass_data_t, 16);
				pass->header.command = RENDER_COMMAND_END_PASS;
				pass->header.size = sizeof(*pass);
			}
			else if(sexp_t* elem = tagged_list(command, "query"))
			{
				size_t num_subsystems = 0;
				if(sexp_t* subsystems = assoc(elem, "subsystems"))
				{
					while(!sexp_is_nil(subsystems))
					{
						subsystems = sexp_cdr(subsystems);
						++num_subsystems;
					}
				}

                size_t size = sizeof(renderc_render_command_query_data_t) + num_subsystems * sizeof(uint32_t);
				renderc_render_command_query_data_t* query = (renderc_render_command_query_data_t*)ALLOCATOR_ALLOC(incheap, size, 16);
				query->header.command = RENDER_COMMAND_QUERY;
				query->header.size = size;

				if(sexp_t* pipeline_set = assoc(elem, "pipeline-set"))
					query->pipeline_set_name_hash = hash_string(sexptos(first(pipeline_set)));

				if(sexp_t* state_block = assoc(elem, "state-block"))
					query->state_block_name_hash = hash_string(sexptos(first(state_block)));

				if(sexp_t* and_mask = assoc(elem, "and-mask"))
					query->and_mask = first(and_mask)->fixnum;

				if(sexp_t* not_mask = assoc(elem, "not-mask"))
					query->not_mask = first(not_mask)->fixnum;

				if(sexp_t* view_context = assoc(elem, "view-context"))
					query->view_context_name_hash = hash_string(sexptos(first(view_context)));

				if(sexp_t* subsystems = assoc(elem, "subsystems"))
				{
					size_t i = 0;
					while(!sexp_is_nil(subsystems))
					{
						query->subsystem_name_hashes[i] = hash_string(sexptos(first(subsystems)));
						subsystems = sexp_cdr(subsystems);
						++i;
					}
					query->num_subsystems = i;
				}
			}
			commands = rest(commands);
		}
	}

	file_t* outfile = file_open(outfilename, FILE_MODE_WRITE);
	if(outfile == NULL)
		return -1;

	size_t total_size = allocator_incheap_bytes_consumed(incheap);
	riff_header_t header = {
		{ 'R', 'E', 'N', 'D' },
		(uint32_t)total_size
	};

	file_write(outfile, &header, sizeof(riff_header_t));
	file_write(outfile, allocator_incheap_start(incheap), total_size);
	file_close(outfile);

	allocator_incheap_destroy(incheap);

	return 0;
}

vgpu_device_type_t parse_device_type(const char*  fn)
{
	const char* dot = strrchr(fn, '.');
	const char* underscore = strrchr(fn, '_');

	if (strncmp(underscore + 1, "null", dot - underscore - 1) == 0)
		return VGPU_DEVICE_NULL;
	else if (strncmp(underscore + 1, "dx11", dot - underscore - 1) == 0)
		return VGPU_DEVICE_DX11;
	else if (strncmp(underscore + 1, "dx12", dot - underscore - 1) == 0)
		return VGPU_DEVICE_DX12;
	else if (strncmp(underscore + 1, "gl", dot - underscore - 1) == 0)
		return VGPU_DEVICE_GL;
	else if (strncmp(underscore + 1, "vk", dot - underscore - 1) == 0)
		return VGPU_DEVICE_VK;

	ERROR_AND_FAIL_EXIT("could not parse gpu device type from shader name %s", fn);
	return VGPU_DEVICE_NULL; 
}

vgpu_program_type_t parse_program_type(const char*  fn)
{
	const char* dot = strrchr(fn, '.');

	if (strcmp(dot + 1, "vp") == 0)
		return VGPU_VERTEX_PROGRAM;
	if (strcmp(dot + 1, "fp") == 0)
		return VGPU_FRAGMENT_PROGRAM;

	ERROR_AND_FAIL_EXIT("could not parse gpu program type from shader name %s", fn);
	return (vgpu_program_type_t)-1;
}

int compile_program_combine(const char* outfilename, const char** infilenames, size_t num_infilenames)
{
	struct data_t
	{
		void* data;
		uint64_t size;
		uint64_t device_type;
		uint64_t program_type;
	};

	array_t<data_t> indatas(&allocator_default, num_infilenames);
	for (size_t i = 0; i < num_infilenames; ++i)
	{
		data_t data;
		data.data = file_open_read_all(infilenames[i], &allocator_default, &data.size);

		data.device_type = parse_device_type(infilenames[i]);
		data.program_type = parse_program_type(infilenames[i]);

		indatas.append(data);
	}

	file_t* outfile = file_open(outfilename, FILE_MODE_WRITE);
	if (outfile == NULL)
		return -1;

	uint64_t num_chunks = num_infilenames;
	file_write(outfile, &num_chunks, sizeof(uint64_t));

	const uint64_t dummy = 0;
	for (size_t i = 0; i < indatas.length(); ++i)
	{
		file_write(outfile, &indatas[i].size, sizeof(uint64_t));
		file_write(outfile, &indatas[i].device_type, sizeof(uint64_t));
		file_write(outfile, &indatas[i].program_type, sizeof(uint64_t));
		file_write(outfile, indatas[i].data, indatas[i].size);
		file_write(outfile, &dummy, ALIGN_UP(indatas[i].size, sizeof(uint64_t)) - indatas[i].size);
	}

	file_close(outfile);

	for (size_t i = 0; i < indatas.length(); ++i)
		ALLOCATOR_FREE(&allocator_default, indatas[i].data);

	return 0;
}

void print_help(getopt_context_t* ctx)
{
	char buffer[2048];
	printf("usage: renderc [options] file\n\n");
	printf("%s", getopt_create_help_string(ctx, buffer, sizeof(buffer)));
}

int main(int argc, const char** argv)
{
	static const getopt_option_t option_list[] =
	{
		{ "help",   'h', GETOPT_OPTION_TYPE_NO_ARG,   0x0, 'h', "displays this message", 0x0 },
		{ "output", 'o', GETOPT_OPTION_TYPE_REQUIRED, 0x0, 'o', "output to file", "file" },
		{ "type",   't', GETOPT_OPTION_TYPE_REQUIRED, 0x0, 't', "data type", "{state_block, render_target, render_pass, pipeline, pipeline_set, render_script, program_combine}" },
		GETOPT_OPTIONS_END
	};

	getopt_context_t go_ctx;
	getopt_create_context(&go_ctx, argc, argv, option_list);

	bool state_block = false;
	bool render_target = false;
	bool render_pass = false;
	bool pipeline = false;
	bool pipeline_set = false;
	bool render_script = false;
	bool program_combine = false;
	size_t curr_infilename = 0;
	const char* infilenames[128] = { NULL };
	const char* outfilename = NULL;

	int opt;
	while( (opt = getopt_next( &go_ctx ) ) != -1 )
	{
		switch(opt)
		{
			case 'h': print_help(&go_ctx); return 0;
			case 'o':
				if(outfilename != NULL && outfilename[0] != '\0')
					ERROR_AND_FAIL("output-file already set to: \"%s\", trying to set it to \"%s\"", outfilename, go_ctx.current_opt_arg);

				outfilename = go_ctx.current_opt_arg;
				break;
			case 't':
				if(state_block || pipeline || pipeline_set || render_script)
					ERROR_AND_FAIL("type already set");
				if(strcmp(go_ctx.current_opt_arg, "state_block") == 0)
					state_block = true;
				else if (strcmp(go_ctx.current_opt_arg, "render_target") == 0)
					render_target = true;
				else if (strcmp(go_ctx.current_opt_arg, "render_pass") == 0)
					render_pass = true;
				else if(strcmp(go_ctx.current_opt_arg, "pipeline") == 0)
					pipeline = true;
				else if(strcmp(go_ctx.current_opt_arg, "pipeline_set") == 0)
					pipeline_set = true;
				else if(strcmp(go_ctx.current_opt_arg, "render_script") == 0)
					render_script = true;
				else if (strcmp(go_ctx.current_opt_arg, "program_combine") == 0)
					program_combine = true;
				break;
			case '+':
				if(curr_infilename >= ARRAY_LENGTH(infilenames))
					ERROR_AND_FAIL("too many input files specified");
				infilenames[curr_infilename++] = go_ctx.current_opt_arg;
				break;
			case 0: break; // ignore, flag was set!
		}
	}

	if(curr_infilename == 0)
		ERROR_AND_FAIL("no input file(s) set");

	if(state_block)
	{
		if (curr_infilename != 1)
			ERROR_AND_FAIL("multiple input files encountered, expected only one");
		return compile_state_block(outfilename, infilenames[0]);
	}
	else if (render_target)
	{
		if (curr_infilename != 1)
			ERROR_AND_FAIL("multiple input files encountered, expected only one");
		return compile_render_target(outfilename, infilenames[0]);
	}
	else if (render_pass)
	{
		if (curr_infilename != 1)
			ERROR_AND_FAIL("multiple input files encountered, expected only one");
		return compile_render_pass(outfilename, infilenames[0]);
	}
	else if(pipeline)
	{
		if (curr_infilename != 1)
			ERROR_AND_FAIL("multiple input files encountered, expected only one");
		return compile_pipeline(outfilename, infilenames[0]);
	}
	else if(pipeline_set)
	{
		if (curr_infilename != 1)
			ERROR_AND_FAIL("multiple input files encountered, expected only one");
		return compile_pipeline_set(outfilename, infilenames[0]);
	}
	else if(render_script)
	{
		if (curr_infilename != 1)
			ERROR_AND_FAIL("multiple input files encountered, expected only one");
		return compile_render_script(outfilename, infilenames[0]);
	}
	else if (program_combine)
	{
		return compile_program_combine(outfilename, infilenames, curr_infilename);
	}

	printf("Please specify a program type using -t {state_block, pipeline, pipeline_set, render_script, program_combine}\n");
	return -1;
}
