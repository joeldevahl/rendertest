#pragma once

#include <core/types.h>
#include <vgpu.h>
#include <render/types.h>
#include <render/rendercommand.h>

typedef vgpu_state_t renderc_state_block_data_t;

struct renderc_render_target_data_t
{
	uint32_t flags;
	uint16_t width;
	uint16_t height;
	uint16_t format;
};

struct renderc_render_pass_part_data_t
{
	uint32_t render_target_name_hash;
};

struct renderc_render_pass_data_t
{
	size_t num_color_parts;
	renderc_render_pass_part_data_t color_parts[16];
	renderc_render_pass_part_data_t depth_stencil_part;
};

struct renderc_pipeline_data_t
{
	uint32_t vp_name_hash;
	uint32_t fp_name_hash;
	vgpu_primitive_type_t prim_type;
};

struct renderc_pipeline_set_part_data_t
{
	uint64_t flags;
	uint32_t pipeline_name_hash;
};

struct renderc_pipeline_set_data_t
{
	size_t num_parts;
	renderc_pipeline_set_part_data_t parts[];
};

struct renderc_render_command_begin_pass_data_t
{
	render_command_header_t header;
	uint32_t render_pass_name_hash;
};

struct renderc_render_command_end_pass_data_t
{
	render_command_header_t header;
};

struct renderc_render_command_query_data_t
{
	render_command_header_t header;

	uint32_t pipeline_set_name_hash;
	uint32_t state_block_name_hash;

	uint32_t and_mask;
	uint32_t not_mask;

	uint32_t view_context_name_hash;

	size_t num_subsystems;
	uint32_t subsystem_name_hashes[];
};
