Unit:Using("io")
Unit:Using("sexp")
Unit:Using("getopt")
Unit:UsingHeaders("riff")
Unit:UsingHeaders("vgpu")
Unit:UsingHeaders("render")

function Unit.Init(self)
	self.executable = true
	self.targetname = "renderc"
	self.ignore_application_lib = true
end

function CompileGPUProgramBundle(settings, ...)
	local infiles = {}
	local outfile = ""
	for f in TableWalk({...}) do
		table.insert(infiles, f)

		local tmp = f
		local _, _, extension = string.find(tmp, ".*%.(.*)")
		tmp = string.gsub(tmp, "." .. extension, "")
		local _, _, name = string.find(tmp, ".*%_(.*)")
		tmp = string.gsub(tmp, "_" .. name, "")
		outfile = tmp .. "." .. extension .. "b"
	end
		
	local renderc = settings.renderc
	local exe = renderc.exe
	local cmd = exe .. " -t program_combine -o " .. outfile
	for f in TableWalk(infiles) do
		cmd = cmd .. " " .. f
	end

	AddJob(outfile, "program_combine " .. outfile, cmd, exe, infiles)
	return outfile
end

function Unit.AddTools(self)
	AddTool(function (settings)
		settings.renderc = {}
		settings.renderc.exe = GetHostBinary("renderc")

		local gen_compile_func = function(settings, var)
			settings.compile.mappings[var] = function (settings, infile)
				local outfile = PathJoin(target.outdir, PathBase(infile)) .. "." .. var
				local renderc = settings.renderc
				local exe = renderc.exe
				local cmd = exe .. " -t " .. var .. " -o " .. outfile .. " " .. infile

				AddJob(outfile, var .. " " .. infile, cmd)
				AddDependency(outfile, renderc.exe)
				AddDependency(outfile, infile)

				return outfile
			end
		end

		gen_compile_func(settings, "state_block")
		gen_compile_func(settings, "render_target")
		gen_compile_func(settings, "render_pass")
		gen_compile_func(settings, "pipeline")
		gen_compile_func(settings, "pipeline_set")
		gen_compile_func(settings, "render_script")
	end)
end

function Unit.Build(self)
	local bin_obj = Compile(self.settings, PathJoin(self.path, "src/renderc.cpp"))
	local bin = Link(self.settings, self.targetname, bin_obj)
	self:AddProduct(bin)
end
