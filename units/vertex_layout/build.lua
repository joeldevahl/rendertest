Unit:Using("dl")

function Unit.Init(self)
	DefaultInit(self)
	self.static_library = false
end

function Unit.AddTools(self)
	AddTool(function (settings)
		settings.vertex_layoutc = {}
		settings.vertex_layoutc.libname = "units/vertex_layout/types/vertex_layout.tlb"

		settings.compile.mappings["vl"] = function (settings, infile)
			local outfile = PathJoin(target.outdir, PathBase(infile)) .. ".vl"
			local libname = PathJoin(target.outdir, settings.vertex_layoutc.libname)
			return DLPack(settings, infile, outfile, libname)
		end
	end)
end

function Unit.BuildTarget(self)
	local typelib_src = Collect(self.path .. "/types/*.tld")
	local typelib = Compile(self.settings, typelib_src);
	self:AddProduct(typelib)
end
