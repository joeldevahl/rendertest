Unit:Using("io")
Unit:Using("dl")
Unit:Using("getopt")

function Unit.Init(self)
	self.executable = true
	--self.static_library = true
	self.targetname = "model"
	self.ignore_application_lib = true
end

function Unit.AddTools(self)
	AddTool(function (settings)
		settings.modelc = {}
		settings.modelc.exe = GetHostBinary("modelc")

		settings.compile.mappings["model"] = function (settings, infile)
			local outfile = PathJoin(target.outdir, PathBase(infile)) .. ".bmodel"
			local modelc = settings.modelc
			local exe = modelc.exe
			local cmd = exe .. " -o " .. outfile .. " " .. infile

			AddJob(outfile, "model " .. infile, cmd)
			AddDependency(outfile, modelc.exe)
			AddDependency(outfile, infile)

			local basepath = "local/build/" .. target.name
			for line in io.lines(infile) do
				local dep_type, file = string.match(line, "//%s*dep%s+(.+)%s+(.+)")
				if dep_type and file then
					if dep_type == "source" then
						AddDependency(outfile, file)
					elseif dep_type == "compiled" then
						AddDependency(outfile, PathJoin(basepath, file))
					end
				end
			end

			return outfile
		end
	end)
end

function Unit.BuildTarget(self)
	local typelib_src = Collect(self.path .. "/types/*.tld")
	local typelib = Compile(self.settings, typelib_src);
	self:AddProduct(typelib)
end

function Unit.Build(self)
		local bin_obj = Compile(self.settings, PathJoin(self.path, "src/modelc.cpp"))
		local bin = Link(self.settings, self.targetname .. "c", bin_obj)
		self:AddProduct(bin)
end
